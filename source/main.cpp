#include "main.h"

static int script_loop()
{
    // Thanks to the commit of cifs: https://github.com/jasonroelofs/rice/pull/137
    int argc = 0;
    char *argv = (char *)malloc(1);
    argv[0] = 0;
    char **pArgv = &argv;

    ruby_sysinit(&argc, &pArgv);
    RUBY_INIT_STACK;
    ruby_init();
    ruby_init_loadpath();
    // By using code above the ruby is well initialized and can be compiled as window applicaction

    rb_call_builtin_inits();
    RGSSWrapper::initialize();

    int ruby_state = 0;
    rb_protect(rb_require_string, Rice::detail::to_ruby("./ruby/main"), &ruby_state);

    if (ruby_state)
    {
        auto rbError = rb_funcall(rb_gv_get("$!"), rb_intern("message"), 0);
        cen::log::error(rb_string_value_ptr(&rbError));
    };

    ruby_cleanup(ruby_state);
    program_terminate = true;
    cv_script_pause.notify_all();
    cv_render_pause.notify_all();

    return 0;
}

static int renderder_loop(void *data)
{
    GM.run();
    return 0;
}

int main(int argc, char *argv[])
{
    cen::library centurion;
    cen::set_hint<cen::hint::render_driver, cen::hint_priority::override>(cen::hint::render_driver::value::direct3d);

    auto [window, renderer] = cen::make_window_and_renderer({640, 480});

    GM.setup(window, renderer);
    cen::thread renderer_core(renderder_loop, "renderer_loop", (void *)NULL);

    window.show();
    // event poll must in main thread.
    script_loop();
    renderer_core.join();
    GM.finalize();

    return 0;
}

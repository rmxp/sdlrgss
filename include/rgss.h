#pragma once
#include "core.h"
#include "command_pipeline.h"

#define VALUEx1 VALUE
#define VALUEx2 VALUE, VALUE
#define VALUEx3 VALUE, VALUE, VALUE
#define VALUEx4 VALUE, VALUE, VALUE, VALUE
#define VALUEx5 VALUE, VALUE, VALUE, VALUE, VALUE
#define VALUEx6 VALUE, VALUE, VALUE, VALUE, VALUE, VALUE
#define VALUEx7 VALUE, VALUE, VALUE, VALUE, VALUE, VALUE, VALUE
#define VALUEx8 VALUE, VALUE, VALUE, VALUE, VALUE, VALUE, VALUE, VALUE

namespace RGSS
{

    struct Color
    {
        int red, green, blue, alpha;

        Color();
        Color(int, int, int, int);
        Color *set(int, int, int, int);

        static void set_red(VALUEx2);
        static void set_green(VALUEx2);
        static void set_blue(VALUEx2);
        static void set_alpha(VALUEx2);
        static void set_attrs(VALUEx5);
        static void init_wrapper();
    };

    struct Tone
    {
        int red, green, blue, gray;

        Tone();
        Tone(int, int, int, int);
        Tone *set(int, int, int, int);

        static void set_red(VALUEx2);
        static void set_green(VALUEx2);
        static void set_blue(VALUEx2);
        static void set_gray(VALUEx2);
        static void set_attrs(VALUEx5);
        static void init_wrapper();
    };

    struct Rect
    {
        int x, y, width, height;
        Rect();
        Rect(int, int, int, int);
        Rect *set(int, int, int, int);

        static void set_x(VALUEx2);
        static void set_y(VALUEx2);
        static void set_width(VALUEx2);
        static void set_height(VALUEx2);
        static void set_attrs(VALUEx5);
        static void init_wrapper();
    };

    struct Table
    {
        int dimension, xsize, ysize, zsize, size;
        std::vector<int16_t> data;

        Table();
        Table(int, int, int);

        Table(std::string);
        std::string _dump(int);

        Table *resize(int, int, int);
        int16_t get_index(int);
        int16_t set_index(int, int16_t);

        static void set_size(VALUEx4);
        static VALUE get(VALUEx2);
        static void set(VALUEx3);
        static void init_wrapper();
    };

    struct Font
    {
        Font(std::string, int);
        ~Font();

        bool bold;
        bool italic;
        int size;
        std::string path;

        Color *color;

        static std::string default_path;
        static int default_size;
        static bool default_bold;
        static bool default_italic;
        static Color default_color;

        static void set_path(VALUE, std::string);
        static void set_size(VALUEx2);
        static void set_bold(VALUEx2);
        static void set_italic(VALUEx2);
        static void set_color(VALUEx2);
        static void init_wrapper();
    };

    struct Bitmap
    {
        uintptr_t id;
        bool disposed;

        int width;
        int height;

        Font *font;

        Bitmap();
        ~Bitmap();

        void setup(std::string, int, int);
        void setup_target(int, int);
        void setup_clone(Bitmap *);
        void setup_autotile(Bitmap *);
        bool is_disposed();
        void dispose();

        void blt(int, int, Bitmap *, Rect *, int);
        void stretch_blt(Rect *, Bitmap *, Rect *, int);
        void fill_rect(int, int, int, int, Color *);
        // void fill_rect2(Rect *, Color *);
        void clear();
        void draw_text(int, int, int, int, std::string, int);
        int text_size(std::string);
        void hue_change(int);
        Bitmap *clone();

        static void _setup_path(VALUEx1, std::string, VALUEx2);
        static void _setup_canvas(VALUEx3);
        static void _dispose(VALUEx1);
        static void _blt(VALUEx6);
        static void _stretch_blt(VALUEx5);
        static void _fill_rect(VALUEx6);
        static void _clear(VALUEx1);
        static void _draw_text(VALUEx5, std::string, VALUEx1);
        static void _text_size(VALUE, std::string);
        static void _hue_change(VALUEx2);
        static void init_wrapper();

    private:
        bool is_target;
    };

    struct Palette
    {
        uintptr_t id;
        bool disposed;
        int width;
        int height;

        cen::surface *surface;

        Palette();
        ~Palette();

        void setup(std::string);
        void setup2(int, int);
        bool is_disposed();
        void dispose();

        Color get_pixel(int, int);
        void set_pixel(int, int, Color);
    };

    struct Autotiles
    {
        std::array<Bitmap *, 8> data;
        Autotiles();
        ~Autotiles();

        Bitmap *get(int);
        void set(int, Bitmap *);
    };

    struct Viewport;

    struct Drawable
    {
        uintptr_t id;
        bool disposed;
        bool visible;
        int z;
        Viewport *viewport;

        Drawable();
        ~Drawable();

        void dispose();
        bool is_disposed();
        int set_z(int);

        virtual bool is_visible();
    };

    struct Viewport : public Drawable
    {
        int ox;
        int oy;
        Color flash_color;
        int flash_type;
        int flash_count;

        Rect *rect;
        Color *color;
        Tone *tone;

        Viewport();
        ~Viewport();

        void setup(int, int, int, int);

        bool is_visible();
        void flash(Color color, int);
        void flash2(int);
        void update();
    };

    struct Sprite : public Drawable
    {
        int x;
        int y;
        int ox;
        int oy;
        double zoom_x;
        double zoom_y;
        double angle;
        bool mirror;
        int bush_depth;
        int opacity;
        int blend_type;
        Color flash_color;
        int flash_count;
        int flash_type;

        Bitmap *bitmap;
        Rect *src_rect;
        Color *color;
        Tone *tone;

        Sprite(std::optional<Viewport *>);
        ~Sprite();

        bool is_visible();

        void set_bitmap(Bitmap *);
        void flash(Color, int);
        void flash2(int);
        void update();

        void set_opacity(int);
    };

    struct Plane : public Drawable
    {
        int ox;
        int oy;
        double zoom_x;
        double zoom_y;
        int opacity;
        int blend_type;

        Bitmap *bitmap;
        Color *color;
        Tone *tone;

        Plane(std::optional<Viewport *>);
        ~Plane();

        bool is_visible();
        void set_bitmap(Bitmap *);

        void set_opacity(int);
    };

    struct Window : public Drawable
    {

        bool stretch;
        bool active;
        bool pause;
        int x;
        int y;
        int width;
        int height;

        int ox;
        int oy;
        int opacity;
        int back_opacity;
        int contents_opacity;

        int update_counts;
        int cursor_counts;

        Bitmap *window_skin;
        Bitmap *contents;
        Rect *cursor_rect;

        Window(std::optional<Viewport *>);
        ~Window();

        bool is_visible();
        void set_windowskin(Bitmap *);
        void set_contents(Bitmap *);
        void update();

        void set_opacity(int);
        void set_back_opacity(int);
        void set_contents_opacity(int);
    };

    struct Tilemap : public Drawable
    {

        int ox;
        int oy;

        Table *map_data;
        Table *flash_data;
        Table *priorities;
        Bitmap *tileset;
        Autotiles *autotiles;

        int update_counts;

        Tilemap(std::optional<Viewport *>);
        ~Tilemap();

        bool is_visible();
        void set_tileset(Bitmap *);
        void set_map_data(Table *);
        void set_flash_data(Table *);
        void set_priorities(Table *);
        void update();
    };

    // 加法：s.rgb + d.rgb = [1, +, 1] [0, +, 1]
    // screen：s.rgb + (1-s.rgb)*d.rgb = [1, +, 1-S] [0, +, 1]，保证透明度跟底层一样
    // 反号乘法：(1-s.rgb)*d.rgb = [0, +, 1-S] [0, +, 1]，保证透明度跟底层一样
    // 减法：
    // 颜色、灰度：s.a*s.rgb + (1-s.a)*d.rgb = [S.a, +, 1-S.a] [0, +, 1]，保证透明度跟底层一样
    // 渐变：s.a*s.rgb + (1-s.a)*d.rgb = [S.a, +, 1-S.a] [1, +, 1-S.a] = blend（alpha叠加）
    // Hue：s.rgb，但是使用d.a
    // 为了避免不透明度平方的问题，在viewport绘制到最终窗口上时，少乘一次opacity
    // Alpha叠加 Blend：s.rgb + (1-s.a)*d.rgb = [1, +, 1-S] [1, +, 1-S.a] = blend2
    const SDL_BlendMode Blend_Mode_Add = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ONE, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD);
    const SDL_BlendMode Blend_Mode_Screen = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ONE, SDL_BLENDFACTOR_ONE_MINUS_SRC_COLOR, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD);
    const SDL_BlendMode Blend_Mode_RevMul = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE_MINUS_SRC_COLOR, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD);
    const SDL_BlendMode Blend_Mode_Mix = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_SRC_ALPHA, SDL_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD);
    const SDL_BlendMode Blend_Mode_Blend = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ONE, SDL_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ONE, SDL_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, SDL_BLENDOPERATION_ADD);
    const SDL_BlendMode Blend_Mode_Color = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ONE, SDL_BLENDFACTOR_ZERO, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD);
    const SDL_BlendMode Blend_Mode_Alpha = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ONE, SDL_BLENDFACTOR_ZERO, SDL_BLENDOPERATION_ADD);
    // 只用加法实现减法，下面的1是白色
    // s, d -> a * s + b * d
    // 1, v -> (1 - v) * 1 + 0 * v = 1 - v
    // u, 1 - v -> 1 * u + 1 * (1 - v) = 1 + u - v
    // 1, 1 + u - v = (1 - (1 + u - v)) * 1 + 0 * (1 + u - v) = v - u
    const SDL_BlendMode Blend_Mode_RevAdd = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_ONE_MINUS_DST_COLOR, SDL_BLENDFACTOR_ZERO, SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_ZERO, SDL_BLENDFACTOR_ONE, SDL_BLENDOPERATION_ADD);

};

#undef VALUEx1
#undef VALUEx2
#undef VALUEx3
#undef VALUEx4
#undef VALUEx5
#undef VALUEx6
#undef VALUEx7
#undef VALUEx8

#include "canvas.h"
#include "graphic_manager.h"

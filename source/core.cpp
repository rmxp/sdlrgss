#include "core.h"

bool program_terminate = false;

std::condition_variable cv_script_pause;
std::condition_variable cv_render_pause;
std::mutex pipe_lock;
bool script_pause = false;
bool render_pause = false;

namespace Core
{
    void set_log_level(int level)
    {
        switch (level)
        {
        case 0:
            cen::log::set_priority(cen::log_priority::warn);
            return;
        case 1:
            cen::log::set_priority(cen::log_priority::info);
            return;
        case 2:
            cen::log::set_priority(cen::log_priority::debug);
            return;
        case 3:
            cen::log::set_priority(cen::log_priority::verbose);
            return;
        }
    }
}
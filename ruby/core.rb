# encoding: utf-8
$: << Dir.getwd + "/ruby/lib/ruby/3.0.0/"
$: << Dir.getwd + "/ruby/lib/ruby/3.0.0/x64-mingw32"

require "zlib"
require "stringio"
require "dimensions"
require "win32/api"
require "rpgxp"
require "core/modules"
require "core/builtin"
require "core/drawable"
require "core/test"
require "rubygems"

def force_utf8_encode(obj)
  case obj
  when Array
    obj.each { |item| force_utf8_encode(item) }
  when Hash
    obj.each { |key, item| force_utf8_encode(item) }
  when String
    obj.force_encoding("utf-8")
  else
    if obj.class.name.start_with?("RPG::")
      obj.instance_variables.each do |name|
        item = obj.instance_variable_get(name)
        force_utf8_encode(item)
      end
    end
  end
end

def load_data(filename)
  File.open(Finder.find(filename, :data), "rb") { |f|
    obj = Marshal.load(f)
    force_utf8_encode(obj)
    return obj
  }
end

def save_data(obj, filename)
  File.open(Finder.find(filename, :data), "rb") { |f|
    Marshal.dump(obj, f)
  }
end

class Win32API < Win32::API
  def initialize(*args)
    args << args.shift
    super(*args)
  end
end

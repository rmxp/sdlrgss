target = object/temp.exe
target_windows = object/tempw.exe

dir_header = include
dir_source = source
dir_object = object
program_version = $(shell git describe --abbrev=0 --tags)

# include
include = -I.
include += -I./include
include += -I./apps/ruby/include/ruby-3.0.0
include += -I./apps/ruby/include/ruby-3.0.0/x64-mingw32
include += -I./apps/ruby/msys64/mingw64/include
include += -I./apps/sdl2/SDL2-2.0.16/x86_64-w64-mingw32/include/SDL2
include += -I./apps/sdl2/SDL2_image-2.0.5/x86_64-w64-mingw32/include/SDL2
include += -I./apps/sdl2/SDL2_mixer-2.0.4/x86_64-w64-mingw32/include/SDL2
include += -I./apps/sdl2/SDL2_ttf-2.0.15/x86_64-w64-mingw32/include/SDL2
include += -I./external_include

# library
library = -L.
library += -L./apps/ruby/lib
library += -L./apps/sdl2/SDL2-2.0.16/x86_64-w64-mingw32/lib
library += -L./apps/sdl2/SDL2_image-2.0.5/x86_64-w64-mingw32/lib
library += -L./apps/sdl2/SDL2_mixer-2.0.4/x86_64-w64-mingw32/lib
library += -L./apps/sdl2/SDL2_ttf-2.0.15/x86_64-w64-mingw32/lib

# libs
lib_dynamic = -lx64-msvcrt-ruby300 -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf
lib_static = -lgmp -lstdc++ -lshell32 -luser32 -lws2_32 -liphlpapi -limagehlp -lshlwapi
lib_all = -static -Wl,-Bdynamic $(lib_dynamic) -Wl,-Bstatic $(lib_static)

cc = g++
flags = -pipe -std=c++17 -m64 -O3 -s -fstack-protector-strong -DPROGRAM_VERSION=\"$(program_version)\"

src = $(wildcard $(dir_source)/*.cpp)
obj = $(addprefix $(dir_object)/, $(notdir $(src:.cpp=.o)))
dep = $(obj:.o=.d)

all: $(target) $(target_windows)

$(target_windows): $(obj)
	$(cc) -o $(target_windows) $(obj) $(flags) $(library) $(lib_all) -mwindows

$(target): $(obj)
	$(cc) -o $(target) $(obj) $(flags) $(library) $(lib_all)

-include $(dep)

$(dir_object)/%.o:$(dir_source)/%.cpp Makefile
	$(cc) $(flags) $(include) -MMD -MP -c $< -o $@

.PHONY: clean
clean:
	del /Q $(dir_object)\*

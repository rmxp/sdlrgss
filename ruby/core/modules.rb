# encoding: utf-8
module Audio
  module_function

  def bgm_play(filename, volume = 80, pitch = 100)
    _bgm_play(Finder.find(filename, :audio), volume, pitch)
  end

  def bgs_play(filename, volume = 80, pitch = 100)
    _bgs_play(Finder.find(filename, :audio), volume, pitch)
  end

  def me_play(filename, volume = 80, pitch = 100)
    _me_play(Finder.find(filename, :audio), volume, pitch)
  end

  def se_play(filename, volume = 80, pitch = 100)
    _se_play(Finder.find(filename, :audio), volume, pitch)
  end
end

module Graphics
  module_function

  def transition(duration = 8, filename = "", vague = 40)
    if (filename != "")
      filename = Finder.find(filename, :image)
    end
    _transition(duration, filename, vague)
  end
end

module Input
end

module Finder
  module_function

  Load_Path = {
    font: [nil, "./resource/", "./Fonts", "C:/Windows/Fonts"],
    image: [nil, "./resource/", "C:/Program Files (x86)/RPG Maker XP/RGSS/Standard"],
    audio: [nil, "./resource/", "C:/Program Files (x86)/RPG Maker XP/RGSS/Standard"],
    data: [nil, "./resource/", "C:/Program Files (x86)/RPG Maker XP/System"],
    none: [nil],
  }

  Suffix = {
    font: ["", ".ttf"],
    image: ["", ".png", ".jpg", ".bmp", ".webp"],
    audio: ["", ".wma", ".mp3", ".wav", ".mid", ".ogg"],
    data: ["", ".rxdata"],
    none: [""],
  }

  Cache = {}

  def find(filename, key = :none)
    if Cache[filename]
      return Cache[filename]
    end
    Load_Path[key].each do |directory|
      Suffix[key].each do |extname|
        path = File.expand_path filename + extname, directory
        if File.exist? path
          Cache[filename] = path
          return Cache[filename]
        end
      end
    end
    case key
    when :font
      puts "[警告] 找不到字体: #{filename}"
    else
      raise
    end
    Cache[filename] = nil
  end
end

#include "rgss_module.h"

namespace RGSS
{
    void Audio::bgm_play(std::string filename, int volume, int pitch) {}
    void Audio::bgm_stop() {}
    void Audio::bgm_fade(int time) {}
    void Audio::bgs_play(std::string filename, int volume, int pitch) {}
    void Audio::bgs_stop() {}
    void Audio::bgs_fade(int time) {}
    void Audio::me_play(std::string filename, int volume, int pitch) {}
    void Audio::me_stop() {}
    void Audio::me_fade(int time) {}
    void Audio::se_play(std::string filename, int volume, int pitch) {}
    void Audio::se_stop() {}
}

namespace RGSS
{
#define ModuleAttr(module, key, type, init)   \
    type module::key = init;                  \
    type module::get_##key() { return key; }; \
    void module::set_##key(type value) { key = value; };

    ModuleAttr(Graphics, frame_count, int, 0);
    ModuleAttr(Graphics, frame_rate, int, 40);
    ModuleAttr(Graphics, synchronize, bool, false);
#undef ModuleAttr

    void Graphics::update()
    {
        auto start = clock();
        commit();
        GM.tick(frame_rate);
        if (synchronize)
        {
            sleep();
        }
        frame_count++;
        cen::log::debug("update cost: %2d ms", static_cast<int>(clock() - start));
    }

    void Graphics::commit()
    {
        Pipe_Transaction GM
            << Core::Command_Code::Frame_Commit;
    }

    void Graphics::sleep()
    {
        std::unique_lock<std::mutex> lock(pipe_lock);
        script_pause = true;
        cv_script_pause.wait(
            lock, []
            { return GM.idle() || program_terminate; });
    }

    void Graphics::frame_reset()
    {
        if (!synchronize)
        {
            sleep();
        }
        GM.tick(frame_rate);
    }

    void Graphics::freeze()
    {
        {
            Pipe_Transaction GM
                << Core::Command_Code::Graphics_Freeze;
        }
        update();
    };

    void Graphics::transition(int duration, std::string filename, int vague)
    {
        if (!filename.empty())
        {
            Pipe_Transaction GM
                << Core::Command_Code::Graphics_Transition_Prepare
                << filename;
        }

        int t = 255;
        while (t)
        {
            t -= t / (duration + 1);
            duration--;
            {
                Pipe_Transaction GM
                    << Core::Command_Code::Graphics_Transition_Update
                    << t << vague;
            }
            update();
            Input::update();
        }
    };

}

namespace RGSS
{

    std::map<int, std::vector<cen::key_code>> Input::key2code = {
        {Input::DOWN, {cen::keycodes::down}},
        {Input::LEFT, {cen::keycodes::left}},
        {Input::RIGHT, {cen::keycodes::right}},
        {Input::UP, {cen::keycodes::up}},

        {Input::A, {cen::keycodes::z, cen::keycodes::left_shift}},
        {Input::B, {cen::keycodes::x, cen::keycodes::escape}},
        {Input::C, {cen::keycodes::c, cen::keycodes::space, cen::keycodes::enter}},

        {Input::X, {cen::keycodes::a}},
        {Input::Y, {cen::keycodes::s}},
        {Input::Z, {cen::keycodes::d}},
        {Input::L, {cen::keycodes::q}},
        {Input::R, {cen::keycodes::w}},
    };

    void Input::update()
    {
        Event::update();
    }

    bool Input::is_trigger(int key)
    {
        auto it = key2code.find(key);
        if (it != key2code.end())
        {
            for (auto key_code : it->second)
            {
                if (Event::key_state(key_code) == 0b01)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool Input::is_press(int key)
    {
        auto it = key2code.find(key);
        if (it != key2code.end())
        {
            for (auto key_code : it->second)
            {
                if (Event::key_state(key_code) & 1)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool Input::is_repeat(int key)
    {
        static std::unordered_map<int, int> counts = {};
        if (is_press(key))
        {
            int delta = Graphics::frame_count - counts[key];
            if (delta > 0 && delta < 8)
            {
                return false;
            }
            counts[key] += delta;
            return true;
        }
        else
        {
            counts[key] = 0;
            return false;
        }
    }

    int Input::dir4()
    {
        if (is_press(Input::DOWN))
        {
            return 2;
        }
        if (is_press(Input::LEFT))
        {
            return 4;
        }
        if (is_press(Input::RIGHT))
        {
            return 6;
        }
        if (is_press(Input::UP))
        {
            return 8;
        }
        return 0;
    }

    int Input::dir8()
    {
        if (is_press(Input::DOWN))
        {
            if (is_press(Input::LEFT))
            {
                return 1;
            }
            if (is_press(Input::RIGHT))
            {
                return 3;
            }
            return 2;
        }
        if (is_press(Input::UP))
        {
            if (is_press(Input::LEFT))
            {
                return 7;
            }
            if (is_press(Input::RIGHT))
            {
                return 9;
            }
            return 8;
        }
        if (is_press(Input::LEFT))
        {
            return 4;
        }
        if (is_press(Input::RIGHT))
        {
            return 6;
        }
        return 0;
    }
}
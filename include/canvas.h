#pragma once
#include "rgss.h"

namespace Core
{
    enum class Drawable
    {
        Canvas,
        Sprite,
        Plane,
        Window,
        Tilemap,
    };
};

namespace Core
{
    struct Canvas
    {
        RGSS::Viewport *viewport;
        typedef const std::pair<int, uintptr_t> z_index;
        typedef std::map<z_index, std::pair<Drawable, void *>> canvas_tree;
        canvas_tree tree;

        Canvas(RGSS::Viewport *);
        ~Canvas();

        void insert(z_index, RGSS::Viewport *);
        void insert(z_index, RGSS::Sprite *);
        void insert(z_index, RGSS::Plane *);
        void insert(z_index, RGSS::Window *);
        void insert(z_index, RGSS::Tilemap *);

        void erase(z_index);
        void update(z_index, int);

        Canvas *find_by_viewport(RGSS::Viewport *);
        Canvas *find_by_key(z_index);
    };
}
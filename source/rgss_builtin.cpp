#include "rgss.h"

namespace RGSS
{
    Color::Color() : red(0), green(0), blue(0), alpha(0) {}

    Color::Color(int _red, int _green, int _blue, int _alpha)
    {
        set(_red, _green, _blue, _alpha);
    }

    Color *Color::set(int _red, int _green, int _blue, int _alpha)
    {
        red = _red;
        green = _green;
        blue = _blue;
        alpha = _alpha;
        return this;
    }
}

namespace RGSS
{
    Tone::Tone() : red(0), green(0), blue(0), gray(0) {}
    Tone::Tone(int _red, int _green, int _blue, int _gray)
    {
        set(_red, _green, _blue, _gray);
    }

    Tone *Tone::set(int _red, int _green, int _blue, int _gray)
    {
        red = _red;
        green = _green;
        blue = _blue;
        gray = _gray;
        return this;
    }
}

namespace RGSS
{
    Rect::Rect() : x(0), y(0), width(0), height(0) {}
    Rect::Rect(int _x, int _y, int _width, int _height)
    {
        set(_x, _y, _width, _height);
    }

    Rect *Rect::set(int _x, int _y, int _width, int _height)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        return this;
    }

}

namespace RGSS
{
    Table::Table()
    {
        resize(1, 1, 1);
    }
    Table::Table(int _xsize, int _ysize, int _zsize)
    {
        resize(_xsize, _ysize, _zsize);
    }

    Table *Table::resize(int _xsize, int _ysize, int _zsize)
    {
        xsize = _xsize;
        ysize = _ysize;
        zsize = _zsize;
        size = xsize * ysize * zsize;
        dimension = 3;
        if (zsize == 1)
        {
            dimension--;
        }
        if (ysize == 1)
        {
            dimension--;
        }
        data.resize(size, 0);
        return this;
    }

    int16_t Table::get_index(int index)
    {
        return data[index];
    }

    int16_t Table::set_index(int index, int16_t value)
    {
        data[index] = value;
        return value;
    }

    Table::Table(std::string s)
    {
        memcpy(this, s.c_str(), sizeof(int) * 5);
        auto ptr = (int16_t *)(s.c_str() + sizeof(int) * 5);
        data.resize(size);
        memcpy(&data[0], ptr, size * sizeof(int16_t));
    }

    std::string Table::_dump(int depth)
    {
        std::string ret = std::string((char *)this, sizeof(int) * 5);
        ret.resize(sizeof(int) * 5 + sizeof(int16_t) * size);
        memcpy(&ret[0] + sizeof(int) * 5, (void *)&data[0], sizeof(int16_t) * size);
        return ret;
    }
}

namespace RGSS
{
    std::string Font::default_path = "";
    int Font::default_size = 22;
    bool Font::default_bold = false;
    bool Font::default_italic = false;
    Color Font::default_color = Color(255, 255, 255, 255);

    Font::Font(std::string font_path, int font_size)
    {
        path = font_path;
        size = font_size;
        bold = Font::default_bold;
        italic = Font::default_italic;
        color = new Color(default_color);
    }

    Font::~Font()
    {
        delete color;
    }

};

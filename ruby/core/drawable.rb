# encoding: utf-8
class Bitmap
  attr_reader :width
  attr_reader :height
  # attr_reader :disposed
  # attr_reader :font

  alias _builtin_initialize initialize

  def initialize(path_or_width, height = nil)
    _builtin_initialize
    if height
      @width, @height = path_or_width, height
      _setup_canvas(self, @width, @height)
    else
      path = Finder.find(path_or_width, :image)
      @width, @height = Dimensions.dimensions(path)
      _setup_path(self, path, @width, @height)
    end
    @disposed = false
  end

  # 简单说一下问题，之后再解决
  # instance_variables并没有被rice还原
  # 再一次调用self.contents时，
  # rice生成了新的Bitmap对象，并把指针绑定到Bitmap上
  # 此时新的Bitmap是没有instance_variables的
  # Table可能也有类似的问题，需要想办法测试、解决
  # 目前想到的方法，是设置@contents也是Window的instance_variable
  # 这些内容等到最终全部修改完才能测试！开个新的分支准备

  def dispose
    if !@disposed
      @disposed = true
      _dispose(self)
    end
  end

  def disposed?
    @disposed
  end

  def rect
    Rect.new(0, 0, @width, @height)
  end

  def font=(font)
    self.font.name = font.name
    self.font.size = font.size
    self.font.bold = font.bold
    self.font.italic = font.italic
    self.font.color = font.color
  end

  def text_size(str)
    if str.empty?
      Rect.new(0, 0, 0, 0)
    else
      value = _text_size(self, str)
      Rect.new(0, 0, value >> 16, value & 0xffff)
    end
  end

  def blt(x, y, src_bitmap, rect, opacity = 255)
    _blt(self, x, y, src_bitmap, rect, opacity)
  end

  def strectch_blt(dest_rect, src_bitmap, src_rect, opacity = 255)
    _strectch_blt(self, dest_rect, src_bitmap, src_rect, opacity)
  end

  def fill_rect(*args)
    if args.first.is_a?(Rect)
      rect = args[0]
      _fill_rect(self, rect.x, rect.y, rect.width, rect.height, args[1])
    else
      _fill_rect(self, *args)
    end
  end

  def clear()
    _clear(self)
  end

  def draw_text(*args)
    if args.first.is_a?(Rect)
      rect = args.shift
      _draw_text(self, rect.x, rect.y, rect.width, rect.height, *args)
    else
      _draw_text(self, *args)
    end
  end

  def hue_change(hue)
    _hue_change(self, hue)
  end

  def clone
    b = self._clone
    b.instance_variable_set(:@width, @width)
    b.instance_variable_set(:@height, @height)
    b
  end
end

class Palette
  alias _builtin_initialize initialize

  def initialize(path_or_width, height = nil)
    _builtin_initialize
    if height
      _setup2(path_or_width, height)
    else
      _setup(path_or_width)
    end
  end

  def rect
    Rect.new(0, 0, width, height)
  end
end

class Viewport
  alias _builtin_initialize initialize

  def initialize(*args)
    _builtin_initialize
    if args.first.is_a?(Rect)
      rect = args.first
      _setup(rect.x, rect.y, rect.width, rect.height)
    else
      _setup(*args)
    end
  end

  def flash(color, duration = nil)
    duration ? _flash(color, duration) : _flash2(color)
  end

  def rect=(rect)
    self.rect.set(rect.x, rect.y, rect.width, rect.height)
  end

  def color=(color)
    self.color.set(color.red, color.green, color.blue, color.alpha)
  end

  def tone=(tone)
    self.tone.set(tone.red, tone.green, tone.blue, tone.gray)
  end
end

class Drawable
end

class Sprite
  def flash(color, duration = nil)
    duration ? _flash(color, duration) : _flash2(color)
  end

  def src_rect=(rect)
    self.src_rect.set(rect.x, rect.y, rect.width, rect.height)
  end

  def color=(color)
    self.color.set(color.red, color.green, color.blue, color.alpha)
  end

  def tone=(tone)
    self.tone.set(tone.red, tone.green, tone.blue, tone.gray)
  end
end

class Plane
  def color=(color)
    self.color.set(color.red, color.green, color.blue, color.alpha)
  end

  def tone=(tone)
    self.tone.set(tone.red, tone.green, tone.blue, tone.gray)
  end
end

class Window
  def src_rect=(rect)
    self.src_rect.set(rect.x, rect.y, rect.width, rect.height)
  end
end

class Tilemap
end

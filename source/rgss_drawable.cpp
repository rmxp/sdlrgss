#include "rgss.h"
#define __self__ (uintptr_t) this

namespace RGSS
{
    using Code = Core::Command_Code;
};

namespace RGSS
{
    Drawable::Drawable()
        : disposed(false), visible(true), viewport(nullptr), z(0)
    {
        id = GM.object_id.fetch_add(1);
    }

    Drawable::~Drawable() { dispose(); }

    void Drawable::dispose()
    {
        if (!disposed)
        {
            disposed = true;
            if (viewport)
            {
                Pipe_Transaction GM
                    << Code::DrawableV_Dispose << id
                    << z << viewport->id << viewport->z;
            }
            else
            {
                Pipe_Transaction GM
                    << Code::Drawable_Dispose << id
                    << z;
            }
        }
    }

    bool Drawable::is_disposed() { return disposed; }

    int Drawable::set_z(int _z)
    {
        if (z == _z)
        {
            return z;
        }
        if (viewport)
        {
            Pipe_Transaction GM
                << Code::DrawableV_Set_Z << id
                << z << _z << viewport->id << viewport->z;
        }
        else
        {
            Pipe_Transaction GM
                << Code::Drawable_Set_Z << id
                << z << _z;
        }
        z = _z;
        return z;
    }

    bool Drawable::is_visible() { return !disposed && visible; }
}

namespace RGSS
{
    Viewport::Viewport()
        : Drawable(),
          ox(0), oy(0),
          flash_color(), flash_type(0), flash_count(0)
    {
        cen::log::debug("Viewport [%lld]", id);
        rect = new Rect(0, 0, 0, 0);
        color = new Color();
        tone = new Tone();
    }

    Viewport::~Viewport()
    {
        cen::log::debug("~Viewport [%lld] %s", id, disposed ? "(skip)" : "");
        delete rect;
        delete color;
        delete tone;
    }

    void Viewport::setup(int x, int y, int width, int height)
    {
        rect->x = x;
        rect->y = y;
        rect->width = width;
        rect->height = height;

        Pipe_Transaction GM
            << Code::Viewport_Setup << __self__
            << z << id;
    }

    bool Viewport::is_visible()
    {
        return Drawable::is_visible() && (flash_type == 0 || flash_count == 0);
    }

    void Viewport::flash(Color color, int duration)
    {
        flash_type = 0;
        flash_count = duration;
        flash_color.set(color.red, color.green, color.blue, color.alpha);
    }

    void Viewport::flash2(int duration)
    {
        flash_type = 1;
        flash_count = duration;
    }

    void Viewport::update()
    {
        if (flash_count)
        {
            if (flash_type == 0)
            {
                flash_color.alpha -= flash_color.alpha / (1 + flash_count);
            }
            flash_count -= 1;
            if (flash_count == 0)
            {
                flash_color.set(0, 0, 0, 0);
            }
        }
    }
}

#define SETUP(type)                                    \
    cen::log::debug(#type " [%lld]", id);              \
    if (v.has_value())                                 \
    {                                                  \
        viewport = v.value();                          \
        Pipe_Transaction GM                            \
            << Code::type##V_Setup << __self__         \
            << z << id << viewport->z << viewport->id; \
    }                                                  \
    else                                               \
    {                                                  \
        Pipe_Transaction GM                            \
            << Code::type##_Setup << __self__          \
            << z << id;                                \
    }

namespace RGSS
{
    Sprite::Sprite(std::optional<Viewport *> v)
        : Drawable(),
          bitmap(nullptr),
          x(0), y(0), ox(0), oy(0), zoom_x(1.0), zoom_y(1.0),
          angle(0.0), mirror(false), bush_depth(0), opacity(255),
          blend_type(0),
          flash_color(), flash_type(0), flash_count(0)
    {
        SETUP(Sprite);
        src_rect = new Rect(0, 0, 0, 0);
        color = new Color();
        tone = new Tone();
    }

    Sprite::~Sprite()
    {
        cen::log::debug("~Sprite [%lld] %s", id, disposed ? "(skip)" : "");
        delete src_rect;
        delete color;
        delete tone;
    }

    void Sprite::set_bitmap(Bitmap *b)
    {
        bitmap = b;
        if (bitmap)
        {
            src_rect->set(0, 0, bitmap->width, bitmap->height);
        }
    }

    bool Sprite::is_visible()
    {
        return Drawable::is_visible() && bitmap &&
               src_rect->width > 0 && src_rect->height > 0 &&
               (flash_type == 0 || flash_count == 0);
    }

    void Sprite::flash(Color color, int duration)
    {
        flash_type = 0;
        flash_count = duration;
        flash_color.set(color.red, color.green, color.blue, color.alpha);
    }

    void Sprite::flash2(int duration)
    {
        flash_type = 1;
        flash_count = duration;
    }

    void Sprite::update()
    {
        if (flash_count)
        {
            if (flash_type == 0)
            {
                flash_color.alpha -= flash_color.alpha / (1 + flash_count);
            }
            flash_count -= 1;
            if (flash_count == 0)
            {
                flash_color.set(0, 0, 0, 0);
            }
        }
    }

    void Sprite::set_opacity(int _opacity)
    {
        opacity = std::min(255, std::max(0, _opacity));
    }
}

namespace RGSS
{
    Plane::Plane(std::optional<Viewport *> v)
        : Drawable(),
          bitmap(nullptr), ox(0), oy(0), zoom_x(1.0), zoom_y(1.0),
          opacity(255), blend_type(0)
    {
        SETUP(Plane);
        color = new Color();
        tone = new Tone();
    }

    Plane::~Plane()
    {
        cen::log::debug("~Plane [%lld] %s", id, disposed ? "(skip)" : "");
        delete color;
        delete tone;
    }

    void Plane::set_bitmap(Bitmap *b) { bitmap = b; }

    bool Plane::is_visible() { return Drawable::is_visible() && bitmap && opacity > 0; }

    void Plane::set_opacity(int _opacity)
    {
        opacity = std::min(255, std::max(0, _opacity));
    }
};

namespace RGSS
{
    Window::Window(std::optional<Viewport *> v)
        : Drawable(),
          window_skin(nullptr), contents(nullptr), stretch(true),
          active(true), pause(false),
          x(0), y(0), width(0), height(0), ox(0), oy(0),
          opacity(255), back_opacity(255), contents_opacity(255),
          update_counts(0), cursor_counts(0)
    {
        SETUP(Window);
        cursor_rect = new Rect(0, 0, 0, 0);
    }

    Window::~Window()
    {
        cen::log::debug("~Window [%lld] %s", id, disposed ? "(skip)" : "");
        delete cursor_rect;
    }

    void Window::set_windowskin(Bitmap *b) { window_skin = b; }
    void Window::set_contents(Bitmap *b) { contents = b; }
    bool Window::is_visible() { return Drawable::is_visible() && window_skin && width > 32 && height > 32; }

    void Window::update()
    {
        update_counts++;
        update_counts = update_counts & 31;
        // 暂停标记和cursor_rect的透明度变换是32次update一个循环
        // 暂停标记的每个小标记停留8个update
        if (active)
        {
            cursor_counts++;
            cursor_counts = cursor_counts & 31;
        }
        else
        {
            cursor_counts = 15;
        }
    }

    void Window::set_opacity(int _opacity)
    {
        opacity = std::min(255, std::max(0, _opacity));
    }
    void Window::set_back_opacity(int _opacity)
    {
        back_opacity = std::min(255, std::max(0, _opacity));
    }
    void Window::set_contents_opacity(int _opacity)
    {
        contents_opacity = std::min(255, std::max(0, _opacity));
    }

}

namespace RGSS
{
    Tilemap::Tilemap(std::optional<Viewport *> v)
        : Drawable(),
          tileset(nullptr), map_data(nullptr), flash_data(nullptr),
          priorities(nullptr), ox(0), oy(0), update_counts(0)
    {
        SETUP(Tilemap)
        autotiles = new Autotiles();
    }
    Tilemap::~Tilemap()
    {
        cen::log::debug("~Tilemap [%lld] %s", id, disposed ? "(skip)" : "");
        delete autotiles;
    }

    void Tilemap::set_tileset(Bitmap *b) { tileset = b; }
    void Tilemap::set_map_data(Table *t) { map_data = t; }
    void Tilemap::set_flash_data(Table *t) { flash_data = t; }
    void Tilemap::set_priorities(Table *t) { priorities = t; }

    void Tilemap::update()
    {
        update_counts++;
        if (update_counts == INT32_MAX)
        {
            update_counts = 0;
        }
    }

    bool Tilemap::is_visible() { return Drawable::is_visible() && tileset && map_data && priorities; }
}
#undef SETUP
#undef __self__
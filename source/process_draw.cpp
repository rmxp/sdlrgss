#include "graphic_manager.h"

void Graphics_Manager::draw()
{
    renderer.set_blend_mode(cen::blend_mode::none);
    renderer.clear_with(cen::colors::transparent);
    draw(canvas.tree);
}

void Graphics_Manager::draw(const Core::Canvas::canvas_tree &tree)
{
    for (auto pair : tree)
    {
        switch (pair.second.first)
        {
        case Core::Drawable::Canvas:
            cen::log::verbose("canvas");
            draw((Core::Canvas *)(pair.second.second));
            break;
        case Core::Drawable::Sprite:
            cen::log::verbose("sprite");
            draw((RGSS::Sprite *)(pair.second.second));
            break;
        case Core::Drawable::Plane:
            cen::log::verbose("plane");
            draw((RGSS::Plane *)(pair.second.second));
            break;
        case Core::Drawable::Window:
            cen::log::verbose("window");
            draw((RGSS::Window *)(pair.second.second));
            break;
        case Core::Drawable::Tilemap:
            cen::log::verbose("tilemap");
            draw((RGSS::Tilemap *)(pair.second.second), pair.first.first);
            break;
        }
    }
}

void Graphics_Manager::draw(const Core::Canvas *canvas)
{
    RGSS::Viewport *viewport = canvas->viewport;
    if (!viewport->is_visible())
    {
        return;
    }
    auto rect = viewport->rect;
    auto target = cen::texture(
        renderer, window.get_pixel_format(),
        cen::texture_access::target, {rect->width, rect->height});
    auto last_target = renderer.get_render_target();
    renderer.set_target(target);
    renderer.set_blend_mode(cen::blend_mode::none);
    renderer.clear_with(cen::colors::transparent);
    // draw all components
    draw(canvas->tree);
    // apply viewport effect: tone and color
    apply_tone_gray(*viewport->tone);
    // reset last target
    (last_target) ? renderer.set_target(last_target) : renderer.reset_target();
    SDL_SetTextureBlendMode(target.get(), RGSS::Blend_Mode_Blend);
    renderer.render(target, cen::irect(0, 0, rect->width, rect->height), cen::irect(rect->x, rect->y, rect->width, rect->height));
    // 和Sprite不一样，viewport的color和tone会修改透明像素
    apply_tone_color(*viewport->tone);
    renderer.set_clip(cen::irect(rect->x, rect->y, rect->width, rect->height));
    auto &c = (viewport->color->alpha > viewport->flash_color.alpha) ? (*viewport->color) : (viewport->flash_color);
    apply_color(c);
    renderer.set_clip(std::nullopt);
}
void Graphics_Manager::draw(RGSS::Sprite *sprite)
{
    if (!sprite->is_visible())
    {
        return;
    }
    // src_rect
    auto &texture = textures.at(sprite->bitmap->id);
    const int x = sprite->src_rect->x;
    const int y = sprite->src_rect->y;
    const int width = sprite->src_rect->width;
    const int height = sprite->src_rect->height;
    // modify target size if the sprite is very big
    auto target = cen::texture(
        renderer, window.get_pixel_format(),
        cen::texture_access::target, {width, height});
    auto last_target = renderer.get_render_target();
    // blt texture
    renderer.set_target(target);
    texture.set_blend_mode(cen::blend_mode::none);
    renderer.render(texture, cen::irect(x, y, width, height), cen::irect(0, 0, width, height));
    // bush_depth
    const int bush_depth = std::min(sprite->bush_depth, height);
    if (bush_depth > 0)
    {
        texture.set_alpha(128);
        cen::irect bush_rect{0, height - bush_depth, width, bush_depth};
        renderer.render(texture, bush_rect, bush_rect);
        texture.set_alpha(255);
    }
    // tone
    apply_tone_gray(*sprite->tone);
    apply_tone_color(*sprite->tone);
    // color
    const auto &c = (sprite->color->alpha > sprite->flash_color.alpha) ? (*sprite->color) : (sprite->flash_color);
    apply_color(c);
    // opacity
    target.set_alpha(sprite->opacity);
    // blend type
    switch (sprite->blend_type)
    {
    case 1:
        target.set_blend_mode(cen::blend_mode::add);
        break;
    case 2:
        target.set_blend_mode(cen::blend_mode::add);
        renderer.set_color(cen::colors::white);
        break;
    default:
        target.set_blend_mode(cen::blend_mode::blend);
        break;
    }
    // reset last target
    (last_target) ? renderer.set_target(last_target) : renderer.reset_target();
    // zoom, rotation, filp
    const cen::irect src_rect{0, 0, width, height};
    const cen::frect dst_rect{
        static_cast<float>(sprite->x - sprite->ox * sprite->zoom_x),
        static_cast<float>(sprite->y - sprite->oy * sprite->zoom_y),
        static_cast<float>(width * sprite->zoom_x),
        static_cast<float>(height * sprite->zoom_y)};
    int v_width, v_height;
    if (sprite->viewport)
    {
        v_width = sprite->viewport->rect->width;
        v_height = sprite->viewport->rect->height;
    }
    else
    {
        v_width = window.width();
        v_height = window.height();
    }
    if (sprite->blend_type == 2)
    {
        SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_RevAdd);
        renderer.fill_rect(cen::irect(0, 0, v_width, v_height));
    }
    renderer.render(
        target, src_rect, dst_rect,
        sprite->angle,
        cen::point(static_cast<float>(sprite->x), static_cast<float>(sprite->y)),
        sprite->mirror ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
    if (sprite->blend_type == 2)
    {
        SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_RevAdd);
        renderer.fill_rect(cen::irect(0, 0, v_width, v_height));
    }
}

void Graphics_Manager::draw(RGSS::Plane *plane)
{
    if (!plane->is_visible())
    {
        return;
    }
    auto &texture = textures.at(plane->bitmap->id);
    const int width = texture.width();
    const int height = texture.height();
    // modify target size if the sprite is very big
    auto target = cen::texture(
        renderer, window.get_pixel_format(),
        cen::texture_access::target, {width, height});
    auto last_target = renderer.get_render_target();
    // blt texture
    renderer.set_target(target);
    texture.set_blend_mode(cen::blend_mode::none);
    renderer.render(texture, cen::point(0, 0));
    apply_tone_gray(*plane->tone);
    apply_tone_color(*plane->tone);
    // color
    apply_color(*plane->color);
    // opacity
    target.set_alpha(plane->opacity);

    // blend type
    switch (plane->blend_type)
    {
    case 1:
        target.set_blend_mode(cen::blend_mode::add);
        break;
    case 2:
        target.set_blend_mode(cen::blend_mode::add);
        renderer.set_color(cen::colors::white);
        break;
    default:
        target.set_blend_mode(cen::blend_mode::blend);
        break;
    }
    // reset last target
    (last_target) ? renderer.set_target(last_target) : renderer.reset_target();

    int v_x, v_y, v_width, v_height;
    if (plane->viewport)
    {
        v_width = plane->viewport->rect->width;
        v_height = plane->viewport->rect->height;
    }
    else
    {
        v_width = window.width();
        v_height = window.height();
    }
    const float dst_width = width * plane->zoom_x;
    const float dst_height = width * plane->zoom_y;
    const float dx = fmod(-plane->ox, dst_width);
    const float dy = fmod(-plane->oy, dst_height);
    const int xmin = static_cast<int>(0 - dx / dst_width) - 1;
    const int ymin = static_cast<int>(0 - dy / dst_height) - 1;
    const int xmax = static_cast<int>((v_width - dx) / dst_width) + 1;
    const int ymax = static_cast<int>((v_height - dy) / dst_height) + 1;

    const cen::irect src_rect{0, 0, width, height};
    cen::frect dst_rect{0.0, 0.0, dst_width, dst_height};
    if (plane->blend_type == 2)
    {
        SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_RevAdd);
        renderer.fill_rect(cen::irect(0, 0, v_width, v_height));
    }
    for (int i = xmin; i <= xmax; i++)
    {
        for (int j = ymin; j <= ymax; j++)
        {
            float dst_x = dx + dst_width * i;
            float dst_y = dy + dst_height * j;
            dst_rect.set_position({dst_x, dst_y});
            renderer.render(target, src_rect, dst_rect);
        }
    }
    if (plane->blend_type == 2)
    {
        SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_RevAdd);
        renderer.fill_rect(cen::irect(0, 0, v_width, v_height));
    }
}

void Graphics_Manager::draw(RGSS::Window *window)
{
    if (!window->is_visible())
    {
        return;
    }
    const int width = window->width;
    const int height = window->height;
    const int wx = window->x;
    const int wy = window->y;
    auto windowskin = window->window_skin ? cen::texture_handle(textures.at(window->window_skin->id)) : cen::texture_handle(nullptr);
    auto contents = window->contents ? cen::texture_handle(textures.at(window->contents->id)) : cen::texture_handle(nullptr);
    // 1. 绘制背景图（拉伸或者平铺）
    if (window->window_skin && window->opacity > 0)
    {
        windowskin.set_blend_mode(cen::blend_mode::blend);
        if (window->back_opacity > 0)
        {
            // 为了严谨，窗口周围缩小了 2 像素大小。这是为了能自然地看见圆角形窗口。
            renderer.set_clip(cen::irect(wx + 1, wy + 1, width - 2, height - 2));
            windowskin.set_alpha(window->back_opacity * window->opacity / 255);
            if (window->stretch)
            {
                cen::irect src_rect(0, 0, 128, 128);
                cen::irect dst_rect(wx, wy, width, height);
                renderer.render(windowskin, src_rect, dst_rect);
            }
            else
            {
                cen::irect src_rect(0, 0, 128, 128);
                cen::irect dst_rect(0, 0, 128, 128);
                for (int x = 0; x < width; x += 128)
                {
                    for (int y = 0; y < height; y += 128)
                    {
                        dst_rect.set_position({wx + x, wy + y});
                        renderer.render(windowskin, src_rect, dst_rect);
                    }
                }
            }
        }
        // 2. 绘制背景边框
        // 四边
        windowskin.set_alpha(window->opacity);
        renderer.set_clip(cen::irect(wx + 2, wy, width - 4, height));
        cen::irect src_rect(128 + 16, 0, 32, 16);
        cen::irect dst_rect(0, window->y, 32, 16);
        for (int x = 0; x < width; x += 32)
        {
            dst_rect.set_x(wx + x);
            renderer.render(windowskin, src_rect, dst_rect);
        }
        src_rect.set_y(64 - 16);
        dst_rect.set_y(wy + height - 16);
        for (int x = 0; x < width; x += 32)
        {
            dst_rect.set_x(wx + x);
            renderer.render(windowskin, src_rect, dst_rect);
        }
        src_rect.set_x(128);
        src_rect.set_y(16);
        src_rect.set_width(16);
        src_rect.set_height(32);
        dst_rect.set_x(wx);
        dst_rect.set_width(16);
        dst_rect.set_height(32);
        renderer.set_clip(cen::irect(wx, wy + 2, width, height - 4));
        for (int y = 0; y < height; y += 32)
        {
            dst_rect.set_y(wy + y);
            renderer.render(windowskin, src_rect, dst_rect);
        }
        src_rect.set_x(128 + 64 - 16);
        dst_rect.set_x(wx + width - 16);
        for (int y = 0; y < height; y += 32)
        {
            dst_rect.set_y(wy + y);
            renderer.render(windowskin, src_rect, dst_rect);
        }
        renderer.set_clip(std::nullopt);
        // 四角
        renderer.render(windowskin, cen::irect(128, 0, 16, 16), cen::irect(wx, wy, 16, 16));
        renderer.render(windowskin, cen::irect(128 + 64 - 16, 0, 16, 16), cen::irect(wx + width - 16, wy, 16, 16));
        renderer.render(windowskin, cen::irect(128, 64 - 16, 16, 16), cen::irect(wx, wy + height - 16, 16, 16));
        renderer.render(windowskin, cen::irect(128 + 64 - 16, 64 - 16, 16, 16), cen::irect(wx + width - 16, wy + height - 16, 16, 16));
    }

    // 4. 绘制内容
    if (window->contents)
    {
        if (window->contents_opacity > 0)
        {
            renderer.set_clip(cen::irect(window->x + 16, window->y + 16, width - 32, height - 32));
            contents.set_alpha(window->contents_opacity);
            contents.set_blend_mode(cen::blend_mode::blend);
            renderer.render(contents, cen::point(window->x + 16 - window->ox, window->y + 16 - window->oy));
            renderer.set_clip(std::nullopt);
            contents.set_alpha(255);
        }
    }
    if (window->window_skin)
    {
        windowskin.set_blend_mode(cen::blend_mode::blend);
        // 绘制滚动标记，完全不受窗口透明度的影响
        if (window->contents)
        {
            if (0 - window->ox < 0)
            {
                // 左
                cen::irect src_rect(128 + 16, 24, 8, 16);
                cen::irect dst_rect(window->x + 4, window->y + height / 2 - 8, 8, 16);
                renderer.render(windowskin, src_rect, dst_rect);
            }
            if (contents.width() - window->ox > width - 32)
            {
                // 右
                cen::irect src_rect(128 + 40, 24, 8, 16);
                cen::irect dst_rect(window->x + width - 12, window->y + window->height / 2 - 8, 8, 16);
                renderer.render(windowskin, src_rect, dst_rect);
            }
            if (0 - window->oy < 0)
            {
                // 上
                cen::irect src_rect(128 + 24, 16, 16, 8);
                cen::irect dst_rect(window->x + width / 2 - 8, window->y + 4, 16, 8);
                renderer.render(windowskin, src_rect, dst_rect);
            }
            if (contents.height() - window->oy > height - 32)
            {
                // 下
                cen::irect src_rect(128 + 24, 40, 16, 8);
                cen::irect dst_rect(window->x + width / 2 - 8, window->y + height - 12, 16, 8);
                renderer.render(windowskin, src_rect, dst_rect);
            }
        }
        // 绘制暂停标记，完全不受窗口透明度的影响
        if (window->pause)
        {
            int src_x = (window->update_counts & 0b01000) ? 128 + 32 + 16 : 128 + 32;
            int src_y = (window->update_counts & 0b10000) ? 64 + 16 : 64;
            cen::irect src_rect(src_x, src_y, 16, 16);
            cen::irect dst_rect(window->x + width / 2 - 8, window->y + height - 16, 16, 16);
            renderer.render(windowskin, src_rect, dst_rect);
        }
        // 绘制cursor_rect
        if (window->contents_opacity > 0)
        {
            auto r = window->cursor_rect;
            if (r->width > 2 && r->height > 2)
            {
                int opacity = abs(31 - window->cursor_counts * 2) * 4 + 128;
                windowskin.set_alpha(opacity * window->contents_opacity / 255);
                // 平移
                int dst_x = window->x + r->x + 16;
                int dst_y = window->y + r->y + 16;
                // 中心
                cen::irect src_rect(128 + 1, 64 + 1, 32 - 2, 32 - 2);
                cen::irect dst_rect(dst_x + 1, dst_y + 1, r->width - 2, r->height - 2);
                renderer.render(windowskin, src_rect, dst_rect);
                // 四边
                renderer.render(windowskin, cen::irect(128 + 1, 64, 30, 1), cen::irect(dst_x + 1, dst_y, r->width - 2, 1));
                renderer.render(windowskin, cen::irect(128, 64 + 1, 1, 30), cen::irect(dst_x, dst_y + 1, 1, r->height - 2));
                renderer.render(windowskin, cen::irect(128 + 1, 64 + 31, 30, 1), cen::irect(dst_x + 1, dst_y + r->height - 1, r->width - 2, 1));
                renderer.render(windowskin, cen::irect(128 + 31, 64 + 1, 1, 30), cen::irect(dst_x + r->width - 1, dst_y + 1, 1, r->height - 2));
                // 四角
                renderer.render(windowskin, cen::irect(128, 64, 1, 1), cen::irect(dst_x, dst_y, 1, 1));
                renderer.render(windowskin, cen::irect(128 + 31, 64, 1, 1), cen::irect(dst_x + r->width - 1, dst_y, 1, 1));
                renderer.render(windowskin, cen::irect(128, 64 + 31, 1, 1), cen::irect(dst_x, dst_y + r->height - 1, 1, 1));
                renderer.render(windowskin, cen::irect(128 + 31, 64 + 31, 1, 1), cen::irect(dst_x + r->width - 1, dst_y + r->height - 1, 1, 1));
                windowskin.set_alpha(255);
            }
        }
    }
}

void Graphics_Manager::draw(RGSS::Tilemap *tilemap, const int z)
{
    if (!tilemap->is_visible())
    {
        return;
    }
    // 注意tilemap在加入到canvas tree时，使用了8个不同的z值，分别对应
    // 0 - 优先级为0
    // 1 - 空
    // 2-6 优先级为1-5
    // 7 - flash_color，闪烁色
    int layer = (z - tilemap->z) / 32;
    if (layer == 1)
    {
        return;
    }
    // xmin, xmax, ymin, ymax
    const int ox = tilemap->ox;
    const int oy = tilemap->oy;
    const int xmin = (0 + ox) / 32 - 1;
    const int ymin = (0 + oy) / 32 - 1;
    int xmax, ymax;
    if (tilemap->viewport)
    {
        xmax = (tilemap->viewport->rect->width + ox) / 32 + 1;
        ymax = (tilemap->viewport->rect->height + oy) / 32 + 1;
    }
    else
    {
        xmax = (window.width() + ox) / 32 + 1;
        ymax = (window.height() + oy) / 32 + 1;
    }
    cen::irect src_rect(0, 0, 32, 32);
    cen::irect dst_rect(0, 0, 32, 32);
    const int xsize = tilemap->map_data->xsize;
    const int ysize = tilemap->map_data->ysize;

    // draw color
    if (layer == 7 && tilemap->flash_data)
    {
        for (int i = xmin; i <= xmax; i++)
        {
            for (int j = ymin; j < ymax; j++)
            {
                int index = (j % ysize + ysize) % ysize;
                index *= xsize;
                index += (i % xsize + xsize) % xsize;
                dst_rect.set_position({i * 32 - ox, j * 32 - oy});
                int16_t color = tilemap->flash_data->get_index(index);
                if (color == 0)
                {
                    continue;
                }
                renderer.set_blend_mode(cen::blend_mode::blend);
                uint8_t red = (color & 0xf00) >> 4;
                uint8_t green = (color & 0x0f0);
                uint8_t blue = (color & 0x00f) << 4;
                uint8_t alpha = abs(31 - (tilemap->update_counts & 31) * 2) * 4;
                renderer.set_color({red, green, blue, alpha});
                renderer.fill_rect(dst_rect);
            }
        }
        return;
    }

    // draw tiles
    if (layer != 0)
    {
        layer -= 1;
    }
    for (int i = xmin; i <= xmax; i++)
    {
        for (int j = ymin; j < ymax; j++)
        {
            int index = (j % ysize + ysize) % ysize;
            index *= xsize;
            index += (i % xsize + xsize) % xsize;
            dst_rect.set_position({i * 32 - ox, j * 32 - oy});
            // 实际上有3层图块，暂时先只画第1层
            for (int k = 0; k < 3; k++)
            {
                int tileid = tilemap->map_data->get_index(index + k * xsize * ysize);
                // check if priority is OK
                if (tileid < 48 || tilemap->priorities->get_index(tileid) != layer)
                {
                    continue;
                }
                // draw
                uintptr_t bid;
                if (tileid < 384)
                {
                    // from autotile
                    auto bitmap = tilemap->autotiles->get((tileid / 48) - 1);
                    bid = bitmap->id;
                    int n = bitmap->height / 32;
                    src_rect.set_position({tileid % 48 * 32, (tilemap->update_counts >> 3) % n * 32});
                }
                else
                {
                    // from tileset
                    bid = tilemap->tileset->id;
                    src_rect.set_position({tileid % 8 * 32, (tileid - 384) / 8 * 32});
                }
                auto &texture = textures.at(bid);
                texture.set_blend_mode(cen::blend_mode::blend);
                renderer.render(texture, src_rect, dst_rect);
            }
        }
    }
}

// effects
void Graphics_Manager::apply_tone_gray(const RGSS::Tone &t)
{
    if (t.gray)
    {
        auto gray = renderer.capture(window.get_pixel_format());
        const int size = gray.width() * gray.height();
        auto ptr = (uint8_t *)gray.data();
        for (int i = 0; i < size; i++)
        {
            uint8_t r = *ptr;
            uint8_t g = *(ptr + 1);
            uint8_t b = *(ptr + 2);
            uint8_t k = (r * 38 + g * 75 + b * 15) >> 7;
            *ptr = k;
            *(ptr + 1) = k;
            *(ptr + 2) = k;
            ptr += 4;
        }
        // 1920x1080
        // "(sync) Frames: 3000, FPS: 36.8" with pixel change
        // "(sync) Frames: 3000, FPS: 39.6" w/o  pixel change

        auto texture = cen::texture(renderer, gray);
        texture.set_alpha(t.gray);
        SDL_SetTextureBlendMode(texture.get(), RGSS::Blend_Mode_Color);
        renderer.render(texture, cen::point(0, 0));
    }
}

void Graphics_Manager::apply_tone_color(const RGSS::Tone &t)
{
    if (t.red || t.green || t.blue)
    {
        renderer.set_blend_mode(cen::blend_mode::add);
        renderer.fill_with(cen::color(std::max(0, t.red), std::max(0, t.green), std::max(0, t.blue), 255));
        // 减法要做3步
        int red = std::max(0, -t.red);
        int green = std::max(0, -t.green);
        int blue = std::max(0, -t.blue);
        if (red || green || blue)
        {
            SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_RevAdd);
            renderer.fill_with(cen::colors::white);
            renderer.set_blend_mode(cen::blend_mode::add);
            renderer.fill_with(cen::color(red, green, blue, 255));
            SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_RevAdd);
            renderer.fill_with(cen::colors::white);
        }
    }
}

void Graphics_Manager::apply_color(const RGSS::Color &c)
{
    if (c.alpha && (c.red || c.green || c.blue))
    {
        SDL_SetRenderDrawBlendMode(renderer.get(), RGSS::Blend_Mode_Mix);
        renderer.fill_with(cen::color(c.red, c.green, c.blue, c.alpha));
    }
}
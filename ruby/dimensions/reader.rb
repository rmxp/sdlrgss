require "dimensions/jpeg_scanner"

module Dimensions
  class Reader
    GIF_HEADER = [0x47, 0x49, 0x46, 0x38]
    PNG_HEADER = [0x89, 0x50, 0x4E, 0x47]
    JPEG_HEADER = [0xFF, 0xD8, 0xFF]
    TIFF_HEADER_I = [0x49, 0x49, 0x2A, 0x00]
    TIFF_HEADER_M = [0x4D, 0x4D, 0x00, 0x2A]
    WEBP_HEADER = [0x52, 0x49, 0x46, 0x46]

    attr_reader :type, :width, :height, :angle

    def initialize
      @process = :determine_type
      @type = nil
      @width = nil
      @height = nil
      @angle = nil
      @size = 0
      @data = ""
      # @data.force_encoding("BINARY") if @data.respond_to?(:force_encoding)
      @data.force_encoding("ascii-8bit") if @data.respond_to?(:force_encoding)
    end

    def <<(data)
      if @process
        @data << data
        @size = @data.length
        process
      end
    end

    def process(process = @process)
      send(@process) if @process = process
    end

    def determine_type
      if @size >= 4
        bytes = @data.unpack("C4")

        if match_header(GIF_HEADER, bytes)
          @type = :gif
        elsif match_header(PNG_HEADER, bytes)
          @type = :png
        elsif match_header(JPEG_HEADER, bytes)
          @type = :jpeg
        elsif match_header(TIFF_HEADER_I, bytes) || match_header(TIFF_HEADER_M, bytes)
          @type = :tiff
        elsif match_header(WEBP_HEADER, bytes)
          @type = :webp
        end

        process @type ? :"extract_#{type}_dimensions" : nil
      end
    end

    def extract_gif_dimensions
      if @size >= 10
        @width, @height = @data.unpack("x6v2")
        process nil
      end
    end

    def extract_png_dimensions
      if @size >= 24
        @width, @height = @data.unpack("x16N2")
        process nil
      end
    end

    def extract_jpeg_dimensions
      scanner = JpegScanner.new(@data)
      if scanner.scan
        @width = scanner.width
        @height = scanner.height
        @angle = scanner.angle

        if @angle == 90 || @angle == 270
          @width, @height = @height, @width
        end

        process nil
      end
    rescue JpegScanner::ScanError
    end

    def extract_tiff_dimensions
      scanner = TiffScanner.new(@data)
      if scanner.scan
        @width = scanner.width
        @height = scanner.height
        process nil
      end
    rescue TiffScanner::ScanError
    end

    def extract_webp_dimensions
      StringIO.open(@data) do |f|
        str = f.read(16)
        raise if "RIFF" != str[0...4]
        filesize = str[4...8].unpack("L")[0] + 8
        raise if "WEBP" != str[8...12]
        case str[12...16]
        when "VP8 "
          f.read(7)
          str = f.read(3)
          if str.unpack("C3") == [0x9d, 0x01, 0x2a]
            a, b = f.read(4).unpack("S2")
            @width, @height = [a, b]
          end
        when "VP8L"
          f.read(4)
          str = f.read(1)
          if str.unpack("C1") == [0x2f]
            a, b = f.read(4).unpack("S2")
            @width, @height = [(a & 0x3fff) + 1, (((b << 2) | (a >> 14)) & 0x3fff) + 1]
          end
        when "VP8X"
          f.read(4)
          a, b, c = f.read(6).unpack("S3")
          @width, @height = [(a | ((b & 0xff) << 16)) + 1, ((c << 8) | (b >> 8)) + 1]
        else
          raise
        end
      end
      process nil
    end

    def match_header(header, bytes)
      bytes[0, header.length] == header
    end
  end
end

#include "event.h"

namespace Event
{
    static std::unordered_map<int, int> state = {};

    static void on_quit_event(const cen::quit_event &event)
    {
        cen::log::info("quit event");
        program_terminate = true;
        cv_script_pause.notify_all();
        cv_render_pause.notify_all();
    }

    static void on_window_event(const cen::window_event &event)
    {
    }

    static void on_mouse_button_event(const cen::mouse_button_event &event)
    {
    }

    //                    update release? press?
    // 00 -> no              00   00        01
    // 10 -> no              00   00        01
    // 01 -> trigger / press 11   10        11
    // 11 -> press           11   10        11
    static void on_keyboard_event(const cen::keyboard_event &event)
    {
        cen::log::debug("keyboard event, key = %d", event.key());
        if (event.released())
        {
            int key = static_cast<int>(event.key());
            cen::log::debug("key released, %d", key);
            state[key] = state[key] & 0b10;
        }
        else if (event.pressed())
        {
            int key = static_cast<int>(event.key());
            cen::log::debug("key pressed, %d", key);
            state[key] = state[key] | 0b01;
        }
    }

    event_dispatcher_t create_event_dispatcher()
    {
        event_dispatcher_t dispatcher;
        dispatcher.bind<cen::quit_event>().to<&on_quit_event>();
        dispatcher.bind<cen::window_event>().to<&on_window_event>();
        dispatcher.bind<cen::mouse_button_event>().to<&on_mouse_button_event>();
        dispatcher.bind<cen::keyboard_event>().to<&on_keyboard_event>();

        return std::move(dispatcher);
    }

    static event_dispatcher_t dispatcher = create_event_dispatcher();

    void update()
    {
        for (auto &pair : state)
        {
            pair.second = (pair.second & 1) ? 0b11 : 0b00;
        }

        dispatcher.poll();

        if (program_terminate)
        {
            throw Rice::Exception(rb_eRuntimeError, "program terminate");
        }
    }

    int key_state(const cen::key_code key_code)
    {
        return state[static_cast<int>(key_code)];
    }

}
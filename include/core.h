#pragma once
#include "centurion.hpp"
#include "rice/rice.hpp"
#include "rice/stl.hpp"

#include <windows.h>

#include <atomic>
#include <mutex>
#include <vector>
#include <map>
#include <unordered_map>
#include <random>

extern bool program_terminate;

extern std::condition_variable cv_render_pause;
extern std::condition_variable cv_script_pause;
extern bool script_pause;
extern bool render_pause;
extern std::mutex pipe_lock;

#define Pipe_Transaction std::lock_guard<std::mutex> lock(pipe_lock);

#ifndef PROGRAM_VERSION
#define PROGRAM_VERSION "develop"
#endif

namespace Core
{
    void set_log_level(int);
}

using namespace cen::literals;

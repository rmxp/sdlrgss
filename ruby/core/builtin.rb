#encoding: utf-8

class Color
  attr_reader :red
  attr_reader :green
  attr_reader :blue
  attr_reader :alpha

  alias _builtin_initialize initialize

  def initialize(red, green, blue, alpha = 255)
    _builtin_initialize
    set(red, green, blue, alpha)
  end

  def red=(red)
    if @red != red && red >= 0 && red <= 255
      @red = red
      set_red(self, @red)
    end
  end

  def green=(green)
    if @green != green && green >= 0 && green <= 255
      @green = green
      set_green(self, @green)
    end
  end

  def blue=(blue)
    if @blue != blue && blue >= 0 && blue <= 255
      @blue = blue
      set_blue(self, @blue)
    end
  end

  def alpha=(alpha)
    if @alpha != alpha && alpha >= 0 && alpha <= 255
      @alpha = alpha
      set_alpha(self, @alpha)
    end
  end

  def set(red, green, blue, alpha = 255)
    @red = red
    @green = green
    @blue = blue
    @alpha = alpha
    set_attrs(self, @red, @green, @blue, @alpha)
  end

  def dump(depth = 1)
    [@red, @green, @blue, @alpha].pack("D4")
  end

  def self._load(s)
    obj = self._new
    obj.set(*s.unpack("D4").collect(&:to_i))
    obj
  end

  def clone
    Color.new(@red, @green, @blue, @alpha)
  end

  def inspect
    "#<Color:%d> [%d, %d, %d, %d]" % [object_id, @red, @green, @blue, @alpha]
  end
end

class Tone
  attr_reader :red
  attr_reader :green
  attr_reader :blue
  attr_reader :gray

  alias _builtin_initialize initialize

  def initialize(red, green, blue, gray = 0)
    _builtin_initialize
    set(red, green, blue, gray)
  end

  def red=(red)
    if @red != red && red >= -255 && red <= 255
      @red = red
      set_red(self, @red)
    end
  end

  def green=(green)
    if @green != green && green >= -255 && green <= 255
      @green = green
      set_green(self, @green)
    end
  end

  def blue=(blue)
    if @blue != blue && blue >= -255 && blue <= 255
      @blue = blue
      set_blue(self, @blue)
    end
  end

  def gray=(gray)
    if @gray != gray && gray >= 0 && gray <= 255
      @gray = gray
      set_gray(self, @gray)
    end
  end

  def set(red, green, blue, gray = 0)
    @red = red
    @green = green
    @blue = blue
    @gray = gray
    set_attrs(self, @red, @green, @blue, @gray)
  end

  def dump(depth = 1)
    [@red, @green, @blue, @gray].pack("D4")
  end

  def self._load(s)
    obj = self._new
    obj.set(*s.unpack("D4").collect(&:to_i))
    obj
  end

  def clone
    Tone.new(@red, @green, @blue, @gray)
  end

  def inspect
    "#<Tone:%d> [%d, %d, %d, %d]" % [object_id, @red, @green, @blue, @gray]
  end
end

class Rect
  attr_reader :x
  attr_reader :y
  attr_reader :width
  attr_reader :height

  alias _builtin_initialize initialize

  def initialize(x, y, width, height)
    _builtin_initialize
    set(x, y, width, height)
  end

  def x=(x)
    if @x != x
      @x = x
      set_x(self, @x)
    end
  end

  def y=(y)
    if @y != y
      @y = y
      set_y(self, @y)
    end
  end

  def width=(width)
    if @width != width && width >= 0
      @width = width
      set_width(self, @width)
    end
  end

  def height=(height)
    if @height != height && height >= 0
      @height = height
      set_height(self, @height)
    end
  end

  def set(x, y, width, height)
    @x = x
    @y = y
    @width = width
    @height = height
    set_attrs(self, @x, @y, @width, @height)
  end

  def dump(depth = 1)
    [@x, @y, @width, @height].pack("I4")
  end

  def self._load(s)
    obj = self._new
    obj.set(*s.unpack("I4"))
    obj
  end

  def clone
    Rect.new(@x, @y, @width, @height)
  end

  def inspect
    "#<Rect:%d> [%d, %d, %d, %d]" % [object_id, @x, @y, @width, @height]
  end

  def empty
    set(0, 0, 0, 0)
  end
end

class Table
  attr_reader :xsize
  attr_reader :ysize
  attr_reader :zsize

  alias _builtin_initialize initialize

  def initialize(xsize, ysize = 1, zsize = 1)
    resize(xsize, ysize, zsize)
  end

  def resize(xsize, ysize, zsize)
    @xsize = xsize
    @ysize = ysize
    @zsize = zsize
    set_size(self, xsize, ysize, zsize)
  end

  def [](x, y = 0, z = 0)
    if x >= @xsize || y >= @ysize || z >= @zsize || x < 0 || y < 0 || z < 0
      nil
    else
      index = x + @xsize * (y + @ysize * z)
      get(self, index)
    end
  end

  def []=(x, y = 0, z = 0, value)
    index = x + @xsize * (y + @ysize * z)
    set(self, index, value)
  end

  def inspect
    "#<Table:%d> [%d x %d x %d]" % [object_id, @xsize, @ysize, @zsize]
  end

  def self._load(s)
    obj = self._new(s)
    obj.resize(*s[4...16].unpack("L3"))
    obj
  end

  def clone
    Marshal.load(Marshal.dump(self))
  end
end

class Font
  attr_reader :bold
  attr_reader :italic
  attr_reader :size
  attr_reader :name
  attr_reader :path
  # attr_reader :color

  alias _builtin_initialize initialize

  def initialize(name = @@default_name, size = Font::default_size)
    @name = name
    @path = Finder.find(@name, :font)
    @size = size
    @bold = Font::default_bold
    @italic = Font::default_italic
    _builtin_initialize(@path, @size)
  end

  def name=(args)
    if args.is_a?(String)
      args = [args]
    end
    args.each do |font_name|
      fn = Finder.find(font_name, :font)
      if fn && fn != @path
        @name = font_name
        @path = fn
        set_path(self, fn)
        return
      end
    end
  end

  def size=(size)
    if @size != size
      @size = size
      set_size(self, @size)
    end
  end

  def bold=(bold)
    if @bold != bold
      @bold = bold
      set_bold(self, @bold)
    end
  end

  def italic=(italic)
    if @italic != italic
      @italic = italic
      set_italic(self, @italic)
    end
  end

  def color=(color)
    set_color(self, color)
  end

  class << self
    def default_name
      @@default_name
    end

    def exist?(font_name)
      !!Finder.find(font_name, :font)
    end

    def default_name=(args)
      if args.is_a?(String)
        args = [args]
      end
      args.each do |font_name|
        fn = Finder.find(font_name, :font)
        if fn
          @@default_name = font_name
          self.default_path = fn
          return
        end
      end
      return nil
    end

    def default_color=(color)
      self.default_color.set(color.red, color.green, color.blue, color.alpha)
    end
  end

  Font.default_name = "simhei"
end

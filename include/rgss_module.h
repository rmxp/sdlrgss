#pragma once
#include "core.h"
#include "event.h"
#include "graphic_manager.h"

namespace RGSS
{
    struct Audio
    {
        static void bgm_play(std::string, int, int);
        static void bgm_stop();
        static void bgm_fade(int);
        static void bgs_play(std::string, int, int);
        static void bgs_stop();
        static void bgs_fade(int);
        static void me_play(std::string, int, int);
        static void me_stop();
        static void me_fade(int);
        static void se_play(std::string, int, int);
        static void se_stop();
    };

    struct Graphics
    {
        static void update();
        static void frame_reset();

        static void freeze();
        static void transition(int, std::string, int);

        static int frame_rate;
        static int frame_count;
        static bool synchronize;

        static int get_frame_rate();
        static void set_frame_rate(int);
        static int get_frame_count();
        static void set_frame_count(int);
        static bool get_synchronize();
        static void set_synchronize(bool);

        static void delay();
        static void commit();
        static void sleep();
    };

    struct Input
    {
        static constexpr int DOWN = 2;
        static constexpr int LEFT = 4;
        static constexpr int RIGHT = 6;
        static constexpr int UP = 8;
        static constexpr int A = 11;
        static constexpr int B = 12;
        static constexpr int C = 13;
        static constexpr int X = 14;
        static constexpr int Y = 15;
        static constexpr int Z = 16;
        static constexpr int L = 17;
        static constexpr int R = 18;
        static constexpr int SHIFT = 21;
        static constexpr int CTRL = 22;
        static constexpr int ALT = 23;
        static constexpr int F5 = 25;
        static constexpr int F6 = 26;
        static constexpr int F7 = 27;
        static constexpr int F8 = 28;
        static constexpr int F9 = 29;

        static void update();
        static bool is_trigger(int key);
        static bool is_press(int key);
        static bool is_repeat(int key);
        static int dir4();
        static int dir8();

        static std::map<int, std::vector<cen::key_code>> key2code;
    };
}
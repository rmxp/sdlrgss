#include "rgss.h"

#define __self__ (uintptr_t) this

namespace RGSS
{
    using Code = Core::Command_Code;
};

namespace RGSS
{
    Bitmap::Bitmap()
        : disposed(false), width(0), height(0),
          font(nullptr), is_target(false)
    {
        id = GM.object_id.fetch_add(1);
        cen::log::info("Bitmap [%lld]", id);
        font = new Font(Font::default_path, Font::default_size);
    }
    Bitmap::~Bitmap()
    {
        cen::log::info("~Bitmap [%lld] %s", id, disposed ? "(skip)" : "");
        delete font;
        dispose();
    }

    void Bitmap::setup(std::string path, int _width, int _height)
    {
        is_target = false;
        width = _width;
        height = _height;
        Pipe_Transaction GM
            << Code::Bitmap_Setup << __self__ << id << path;
    }

    void Bitmap::setup_target(int _width, int _height)
    {
        is_target = true;
        width = _width;
        height = _height;
        Pipe_Transaction GM
            << Code::Bitmap_Setup_Target << id << width << height;
    }

    void Bitmap::setup_clone(Bitmap *src_bitmap)
    {
        is_target = true;
        width = src_bitmap->width;
        height = src_bitmap->height;

        auto font_ptr = src_bitmap->font;
        auto color_ptr = font_ptr->color;
        font->path = font_ptr->path;
        // font->name = font_ptr->name;
        font->size = font_ptr->size;
        font->bold = font_ptr->bold;
        font->italic = font_ptr->italic;
        font->color->set(color_ptr->red, color_ptr->green, color_ptr->blue, color_ptr->alpha);

        Pipe_Transaction GM
            << Code::Bitmap_Setup_Clone << id
            << src_bitmap->id << width << height;
    }

    void Bitmap::setup_autotile(Bitmap *src_bitmap)
    {
        is_target = true;
        width = 32 * 48;
        height = src_bitmap->width / 3;

        Pipe_Transaction GM
            << Code::Bitmap_Setup_Autotile << id
            << src_bitmap->id << width << height;
    }

    bool Bitmap::is_disposed() { return disposed; }
    void Bitmap::dispose()
    {
        if (!disposed)
        {
            disposed = true;
            Pipe_Transaction GM
                << Code::Bitmap_Dispose << id;
        }
    }

    void Bitmap::blt(int x, int y, Bitmap *src_bitmap, Rect *src_rect, int opacity)
    {
        if (is_target)
        {
            Pipe_Transaction GM
                << Code::Bitmap_Blt << id
                << x << y
                << src_bitmap->id
                << src_rect->x << src_rect->y << src_rect->width << src_rect->height
                << opacity;
        }
    }

    void Bitmap::stretch_blt(Rect *dest_rect, Bitmap *src_bitmap, Rect *src_rect, int opacity)
    {
        if (is_target)
        {
            Pipe_Transaction GM
                << Code::Bitmap_Stretch_Blt << id
                << dest_rect->x << dest_rect->y << dest_rect->width << dest_rect->height
                << src_bitmap->id
                << src_rect->x << src_rect->y << src_rect->width << src_rect->height
                << opacity;
        }
    }
    void Bitmap::fill_rect(int x, int y, int width, int height, Color *color)
    {
        if (is_target)
        {
            uintptr_t color_rgba = (color->red << 24) | (color->green << 16) | (color->blue << 8) | (color->alpha);
            Pipe_Transaction GM
                << Code::Bitmap_Fill_Rect << id
                << x << y << width << height
                << color_rgba;
        }
    }
    // void Bitmap::fill_rect2(Rect *rect, Color *color)
    // {
    //     fill_rect(rect->x, rect->y, rect->width, rect->height, color);
    // }

    void Bitmap::clear()
    {
        if (is_target)
        {
            Pipe_Transaction GM
                << Code::Bitmap_Clear << id;
        }
    }
    void Bitmap::draw_text(int x, int y, int width, int height, std::string str, int align)
    {
        if (is_target && !str.empty() && width && height)
        {
            int font_config = (font->size << 8) | (font->italic << 1) | (font->bold);
            uintptr_t color_rgba = (font->color->red << 24) | (font->color->green << 16) | (font->color->blue << 8) | (font->color->alpha);
            Pipe_Transaction GM
                << Code::Bitmap_Draw_Text << id
                << x << y << width << height
                << str << align
                << font->path << font_config
                << color_rgba;
        }
    }

    int Bitmap::text_size(std::string str)
    {
        int w, h;

        auto *font_ptr = GM.font_for_script(font->path, font->size);
        TTF_SizeUTF8(font_ptr, str.c_str(), &w, &h);
        return w * 65536 + h;
    }

    void Bitmap::hue_change(int hue)
    {
        if (is_target)
        {
            Pipe_Transaction GM
                << Code::Bitmap_Hue_Change << id
                << hue;
        }
    }

    Bitmap *Bitmap::clone()
    {
        auto bitmap = new Bitmap();
        bitmap->setup_clone(this);
        return bitmap;
    }
}

namespace RGSS
{
    Palette::Palette()
        : disposed(false), width(0), height(0), surface(nullptr)
    {
        id = GM.object_id.fetch_add(1);
        cen::log::info("Palette [%lld]", id);
    }

    Palette::~Palette()
    {
        cen::log::info("~Palette [%lld]", id);
        dispose();
    }

    void Palette::setup(std::string path)
    {
        surface = new cen::surface(path);
        width = surface->width();
        height = surface->height();
    }

    void Palette::setup2(int _width, int _height)
    {
        surface = new cen::surface({_width, _height}, cen::pixel_format::rgba32);
        width = _width;
        height = _height;
    }

    bool Palette::is_disposed() { return disposed; }

    void Palette::dispose()
    {
        if (!disposed)
        {
            disposed = true;
            delete surface;
        }
    }

    void Palette::set_pixel(int x, int y, Color color)
    {
        auto c = cen::color(color.red, color.green, color.blue, color.alpha);
        surface->set_pixel({x, y}, c);
    }

    Color Palette::get_pixel(int x, int y)
    {
        if (x >= width || y > height || x < 0 || y < 0)
        {
            return Color(0, 0, 0, 0);
        }
        auto ptr = (uint8_t *)surface->data();
        ptr += (x + y * width) * 4;
        uint8_t red = *ptr++;
        uint8_t green = *ptr++;
        uint8_t blue = *ptr++;
        uint8_t alpha = *ptr++;
        return Color(red, green, blue, alpha);
    }
}

namespace RGSS
{
    Autotiles::Autotiles()
    {
        for (int i = 0; i < 8; i++)
        {
            data[i] = nullptr;
        }
    }

    Autotiles::~Autotiles()
    {
        for (int i = 0; i < 8; i++)
        {
            if (data[i])
            {
                delete data[i];
            };
        }
    }

    Bitmap *Autotiles::get(int index)
    {
        if (index < 0 && index > 8)
        {
            return nullptr;
        }
        return data[index];
    }

    void Autotiles::set(int index, Bitmap *b)
    {
        if (index < 0 && index > 8)
        {
            return;
        }
        data[index] = new Bitmap();
        data[index]->setup_autotile(b);
    }
}

#undef __self__

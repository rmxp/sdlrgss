#include <centurion.hpp>
#include <rice/rice.hpp>
#include <iostream>
#include <windows.h>

using namespace Rice;

void Init_test()
{
    Class rb_cTest = define_class("Test");
}

int main(int argc, char *argv[])
{
    FreeConsole();

    ruby_init();
    Init_test();

    std::cout << "Hello, rice" << std::endl;
    cen::library centurion;

    auto [window, renderer] = cen::make_window_and_renderer();

    const cen::texture texture{renderer, "hello-texture.png"};
    const cen::font font{"hello-font.ttf", 36};

    // Renders a string to a texture, which in turn can be rendered
    auto text = renderer.render_blended_utf8("Hello world!", font);

    window.show();

    renderer.clear_with(cen::colors::light_coral); // Clear rendering target

    // Draw a filled circle
    renderer.set_color(cen::colors::red);
    renderer.fill_circle(cen::point(150.0, 150.0), 75);

    // Draw an outlined circle
    renderer.set_color(cen::colors::cyan);
    renderer.draw_circle(cen::point(350, 275), 50);

    // Draw a filled rectangle
    renderer.set_color(cen::colors::lime_green);
    renderer.fill_rect(cen::rect(25.0, 500.0, 125.0, 75.0));

    // Draw an outlined rectangle
    renderer.set_color(cen::colors::khaki);
    renderer.draw_rect(cen::rect(550.0, 50.0, 140.0, 60.0));

    // Draw a line between two points
    renderer.set_color(cen::colors::forest_green);
    renderer.draw_line(cen::point(240.0, 340.0), cen::point(360.0, 460.0));

    // Text can be rendered the same way as normal textures
    renderer.render(texture, cen::point(600.0, 450.0));
    renderer.render(text, cen::point(450.0, 230.0));

    renderer.present(); // Apply rendering commands to target

    rb_eval_string("sleep 3");

    window.hide();

    std::cout << "-- end --" << std::endl;
    return ruby_cleanup(0);
}

#include "graphic_manager.h"

Graphics_Manager::Graphics_Manager()
    : renderer(nullptr), window(nullptr), canvas(nullptr),
      transition_freeze_id(0), transition_surface(nullptr),
      timer(), textures(), pipe_script(), pipe_render(),
      object_id(0), render_fonts(), script_fonts()
{
}

Core::Command_Pipeline &Graphics_Manager::operator<<(Core::Command_Code code)
{
    pipe_script.codes.push_back(code);
    if (render_pause)
    {
        render_pause = false;
        cv_render_pause.notify_one();
    }
    return this->pipe_script;
}

Graphics_Manager &Graphics_Manager::getInstance()
{
    static Graphics_Manager instance;
    return instance;
}

void Graphics_Manager::setup(const cen::window &_window, const cen::renderer &_renderer)
{
    renderer = cen::renderer_handle(_renderer);
    window = cen::window_handle(_window);
    cen::log::info("Driver: %s", cen::get_info(renderer).value().name());
}

void Graphics_Manager::run()
{
    while (true)
    {
        if (pipe_script.idle())
        {
            std::unique_lock<std::mutex> lock(pipe_lock);
            if (script_pause)
            {
                script_pause = false;
                cv_script_pause.notify_all();
            }
            render_pause = true;
            cv_render_pause.wait(
                lock, []
                { return !GM.idle() || program_terminate; });
            if (program_terminate)
            {
                return;
            }
            continue;
        }
        else
        {
            Pipe_Transaction;
            std::swap(pipe_script, pipe_render);
        }
        // start processing render commands
        int frame_counts = std::count(pipe_render.codes.begin(), pipe_render.codes.end(), Core::Command_Code::Frame_Commit);
        for (auto code : pipe_render.codes)
        {
            if (program_terminate)
            {
                return;
            }
            if (code == Core::Command_Code::Frame_Commit)
            {
                frame_counts--;
                if (frame_counts > 0)
                    continue;
            }
            cen::log::verbose("Pipe Render: <%d> - Begin", static_cast<int>(code));
            process_render_commands(code);
            cen::log::verbose("Pipe Render: <%d> - End", static_cast<int>(code));
        }
        pipe_render.reset();
    }
}

void Graphics_Manager::finalize()
{
    cv_script_pause.notify_all();
    cv_render_pause.notify_all();
    window.hide();
    render_fonts.clear();
    textures.clear();
    // clear script_fonts release fonts
    for (auto [str, map] : script_fonts)
    {
        for (auto [size, ptr] : map)
        {
            TTF_CloseFont(ptr);
        }
    }
    script_fonts.clear();
}

bool Graphics_Manager::idle()
{
    return pipe_script.idle() && pipe_render.idle();
}

void Graphics_Manager::tick(const int frame_rate)
{
    if (frame_rate)
    {
        timer.tick(1.0 / frame_rate);
    }
}

cen::font &Graphics_Manager::font_for_render(const std::string &path, int size)
{
    auto &m = render_fonts[path];
    auto it = m.find(size);
    if (it != m.end())
    {
        return it->second;
    }
    else
    {
        m.insert({size, std::move(cen::font(path, size))});
        auto it2 = m.find(size);
        return it2->second;
    }
}

TTF_Font *Graphics_Manager::font_for_script(const std::string &path, int size)
{
    auto &m = script_fonts[path];
    auto it = m.find(size);
    if (it != m.end())
    {
        return it->second;
    }
    else
    {
        m.insert({size, TTF_OpenFont(path.c_str(), size)});
        auto it2 = m.find(size);
        return it2->second;
    }
}

# encoding: utf-8

t = Table.new(1024, 1024, 8)
tt = Time.now
t.xsize.times do |i|
  t.ysize.times do |j|
    1.times do |k|
      t[i, j, k]
    end
  end
end
p Time.now - tt # 0.465s

exit()

#pragma once
#include "core.h"

namespace Core
{
    // -----------------------------------------------------
    // Renderer Command
    // -----------------------------------------------------
    enum class Command_Code
    {
        __Empty__,
        Frame_Commit,

        // Bitmap
        Bitmap_Setup,
        Bitmap_Setup_Target,
        Bitmap_Setup_Clone,
        Bitmap_Setup_Autotile,
        Bitmap_Dispose,
        Bitmap_Fill_Rect,
        Bitmap_Clear,
        Bitmap_Blt,
        Bitmap_Stretch_Blt,
        Bitmap_Draw_Text,
        Bitmap_Hue_Change,

        // Viewport
        Viewport_Setup,
        Viewport_Dispose,
        Viewport_Set_Z,

        // Drawable
        Drawable_Setup,
        DrawableV_Setup,
        Drawable_Dispose,
        DrawableV_Dispose,
        Drawable_Set_Z,
        DrawableV_Set_Z,

        // Sprite
        Sprite_Setup,
        SpriteV_Setup,

        // Plane
        Plane_Setup,
        PlaneV_Setup,

        // Window
        Window_Setup,
        WindowV_Setup,

        // Tilemap
        Tilemap_Setup,
        TilemapV_Setup,

        // Graphics
        Graphics_Freeze,
        Graphics_Transition_Prepare,
        Graphics_Transition_Update,
    };
}

// -----------------------------------------------------
// Pipelines
// -----------------------------------------------------
/* pipeline class */
namespace Core
{
    // -----------------------------------------------------
    // Command_Pipeline
    // -----------------------------------------------------
    class Command_Pipeline
    {
    public:
        Command_Pipeline();
        void reset();
        bool idle();
        void run();

        /* args input and output */
        Command_Pipeline &operator<<(uintptr_t);
        Command_Pipeline &operator<<(int);
        Command_Pipeline &operator<<(double);
        Command_Pipeline &operator<<(std::string);
        Command_Pipeline &operator>>(uintptr_t &);
        Command_Pipeline &operator>>(int &);
        Command_Pipeline &operator>>(double &);
        Command_Pipeline &operator>>(std::string &);

        std::vector<Command_Code> codes;

    private:
        std::vector<uintptr_t> args_ptr;
        std::vector<int> args_int;
        std::vector<double> args_double;
        std::vector<std::string> args_string;
        int index_ptr;
        int index_int;
        int index_double;
        int index_string;
    };
}
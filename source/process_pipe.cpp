#include "graphic_manager.h"

static void bitmap_hue_change(uint8_t *data, int size, int hue)
{
    // thanks dongjx for generating the rotation matrix
#define PI 3.141592653589793115997963468544185161590576171875
    double k1 = (1 - cos(hue * PI / 180) + sqrt(3) * sin(hue * PI / 180)) / 3;
    double k2 = (1 - cos(hue * PI / 180) - sqrt(3) * sin(hue * PI / 180)) / 3;
#undef PI
    double k0 = 1 - k1 - k2;

    // do convertion
    uint8_t x, y, z;
    for (int i = 0; i < size / 4; i++)
    {
        x = *(data);
        y = *(data + 1);
        z = *(data + 2);
#define convert(x, y, z) static_cast<uint8_t>(std::min(255, (std::max(0, static_cast<int>(k0 * x + k1 * y + k2 * z)))))
        *(data) = convert(x, y, z);
        *(data + 1) = convert(y, z, x);
        *(data + 2) = convert(z, x, y);
#undef convert
        data += 4;
    }
}

// 版权声明：本文为CSDN博主「gouki04」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
// 原文链接：https://blog.csdn.net/gouki04/article/details/7107088
static const uint32_t Autotile_Map[48] = {
    0x1a1b2021, 0x041b2021, 0x1a052021, 0x04052021,
    0x1a1b200b, 0x041b200b, 0x1a05200b, 0x0405200b,
    0x1a1b0a21, 0x041b0a21, 0x1a050a21, 0x04050a21,
    0x1a1b0a0b, 0x041b0a0b, 0x1a050a0b, 0x04050a0b,
    0x18191e1f, 0x18051e1f, 0x18191e0b, 0x18051e0b,
    0x0e0f1415, 0x0e0f140b, 0x0e0f0a15, 0x0e0f0a0b,
    0x1c1d2223, 0x1c1d0a23, 0x041d2223, 0x041d0a23,
    0x1a1b2c2d, 0x04272c2d, 0x26052c2d, 0x04052c2d,
    0x181d1e23, 0x0e0f2c2d, 0x0c0d1213, 0x0c0d120b,
    0x10111617, 0x10110a17, 0x28292e2f, 0x04292e2f,
    0x24252a2b, 0x24052a2b, 0x0c111217, 0x0c0d2a2b,
    0x24292a2f, 0x10112e2f, 0x0c112a2f, 0x0c112a2f};

// --------------------------------------------------------------- //
// Process Render Commands
// --------------------------------------------------------------- //
void Graphics_Manager::process_render_commands(Core::Command_Code code)
{
#define Pull pipe_render >> (uintptr_t &)
    using Code = Core::Command_Code;
    switch (code)
    {
    case Code::Frame_Commit:
    {
        auto start = clock();
        if (!transition_freeze_id)
        {
            renderer.reset_target();
            draw();
        }
        renderer.present();
        cen::log::debug("render cost: %2d ms", static_cast<int>(clock() - start));
        return;
    }
    case Code::Bitmap_Setup:
    {
        RGSS::Bitmap *bitmap;
        uintptr_t bid;
        std::string path;
        Pull bitmap >> bid >> path;

        auto texture = cen::texture(renderer, path);
        textures.emplace(bid, move(texture));
        return;
    }
    case Code::Bitmap_Setup_Target:
    {
        uintptr_t bid;
        int width, height;
        Pull bid >> width >> height;

        auto texture = cen::texture(
            renderer,
            window.get_pixel_format(),
            cen::texture_access::target,
            {width, height});
        textures.emplace(bid, move(texture));
        return;
    }
    case Code::Bitmap_Setup_Clone:
    {
        uintptr_t bid, src_bid;
        int width, height;
        Pull bid >> src_bid >> width >> height;

        auto texture = cen::texture(
            renderer,
            window.get_pixel_format(),
            cen::texture_access::target,
            {width, height});
        auto source = cen::irect(0, 0, width, height);
        auto destination = cen::irect(0, 0, width, height);
        auto src_texture = cen::texture_handle(textures.at(src_bid));

        renderer.set_target(texture);
        src_texture.set_blend_mode(cen::blend_mode::none);
        renderer.render(src_texture, source, destination);

        textures.emplace(bid, move(texture));
        return;
    }
    case Code::Bitmap_Setup_Autotile:
    {
        uintptr_t bid, src_bid;
        int width, height;
        Pull bid >> src_bid >> width >> height;

        auto texture = cen::texture(
            renderer,
            window.get_pixel_format(),
            cen::texture_access::target,
            {width, height});

        renderer.set_target(texture);
        auto &src_texture = textures.at(src_bid);
        src_texture.set_blend_mode(cen::blend_mode::none);

        cen::irect src_rect(0, 0, 16, 16);
        cen::irect dst_rect(0, 0, 16, 16);
        int sx, sy, dx, dy, index;
        int nrow = src_texture.width() / 96;
        for (int i = 0; i < 48; i++)
        {
            for (int j = 0; j < nrow; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    index = (Autotile_Map[i] >> (24 - 8 * k)) & 255;
                    sx = j * 96 + (index % 6) * 16;
                    sy = index / 6 * 16;
                    dx = i * 32 + ((k & 1) ? 16 : 0);
                    dy = j * 32 + ((k & 2) ? 16 : 0);
                    // draw
                    src_rect.set_position({sx, sy});
                    dst_rect.set_position({dx, dy});
                    renderer.render(src_texture, src_rect, dst_rect);
                }
            }
        }

        textures.emplace(bid, move(texture));
        return;
    }
    case Code::Bitmap_Dispose:
    {
        uintptr_t bid;
        Pull bid;

        textures.erase(bid);
        return;
    }
    case Code::Bitmap_Fill_Rect:
    {
        uintptr_t bid, color_rgba;
        int x, y, width, height;
        Pull bid >> x >> y >> width >> height >> color_rgba;

        uint8_t red = color_rgba >> 24;
        uint8_t green = color_rgba >> 16;
        uint8_t blue = color_rgba >> 8;
        uint8_t alpha = color_rgba >> 0;

        renderer.set_target(textures.at(bid));
        renderer.set_blend_mode(cen::blend_mode::none);
        renderer.set_color(cen::color(red, green, blue, alpha));
        renderer.fill_rect(cen::rect(x, y, width, height));

        return;
    }
    case Code::Bitmap_Clear:
    {
        uintptr_t bid;
        Pull bid;

        renderer.set_target(textures.at(bid));
        renderer.set_blend_mode(cen::blend_mode::none);
        renderer.clear_with(cen::colors::transparent);

        return;
    }
    case Code::Bitmap_Blt:
    {
        uintptr_t bid, src_bid;
        int x, y;
        int src_x, src_y, src_width, src_height;
        int opacity;
        Pull bid >> x >> y >> src_bid >>
            src_x >> src_y >> src_width >> src_height >> opacity;

        renderer.set_target(textures.at(bid));
        auto source = cen::irect(src_x, src_y, src_width, src_height);
        auto destination = cen::irect(x, y, src_width, src_height);
        auto texture = cen::texture_handle(textures.at(src_bid));

        texture.set_blend_mode(cen::blend_mode::blend);
        texture.set_alpha(opacity);
        renderer.render(texture, source, destination);
        texture.set_alpha(255);

        return;
    }
    case Code::Bitmap_Stretch_Blt:
    {
        uintptr_t bid, src_bid;
        int x, y, width, height;
        int src_x, src_y, src_width, src_height;
        int opacity;
        Pull bid >> x >> y >> width >> height >>
            src_bid >> src_x >> src_y >> src_width >> src_height >> opacity;

        renderer.set_target(textures.at(bid));
        auto source = cen::irect(src_x, src_y, src_width, src_height);
        auto destination = cen::irect(x, y, width, height);
        auto texture = cen::texture_handle(textures.at(src_bid));

        texture.set_blend_mode(cen::blend_mode::blend);
        texture.set_alpha(opacity);
        renderer.render(texture, source, destination);
        texture.set_alpha(255);

        return;
    }
    case Code::Bitmap_Draw_Text:
    {
        uintptr_t bid, color_rgba;
        int x, y, width, height, align;
        std::string str, font_path;
        int font_config;
        Pull bid >> x >> y >> width >> height >> str >>
            align >> font_path >> font_config >> color_rgba;

        uint8_t red = color_rgba >> 24;
        uint8_t green = color_rgba >> 16;
        uint8_t blue = color_rgba >> 8;
        uint8_t alpha = color_rgba >> 0;

        // blend_utf8 texture
        bool bold = font_config & 1;
        bool italic = font_config & 2;
        int font_size = font_config >> 8;

        auto &font = font_for_render(font_path, font_size);
        font.set_bold(bold);
        font.set_italic(italic);

        renderer.set_color(cen::color(red, green, blue, alpha));
        auto texture = renderer.render_blended_utf8(str, font);
        // display
        auto _height = texture.height();
        auto _width = texture.width();
        if (_height < height)
        {
            y += (height - _height) / 2;
            height = _height;
        }
        else
        {
            _height = height;
        }
        if (_width < width)
        {
            switch (align)
            {
            default: // left
                break;
            case 1: // center
                x += (width - _width) / 2;
                break;
            case 2: // right
                x += width - _width;
                break;
            }
            width = _width;
        }
        else
        {
            _width = width * 5 / 3;
        }
        // render
        texture.set_blend_mode(cen::blend_mode::none);
        renderer.set_target(textures.at(bid));
        renderer.render(texture, cen::irect(0, 0, _width, _height), cen::irect(x, y, width, height));

        font.reset();
        return;
    }
    case Code::Bitmap_Hue_Change:
    {
        uintptr_t bid;
        int hue;
        Pull bid >> hue;

        renderer.set_target(textures.at(bid));
        auto surface = renderer.capture(window.get_pixel_format());

        int size = surface.width() * surface.height() * 4;
        bitmap_hue_change((uint8_t *)surface.data(), size, hue);
        auto texture = cen::texture(renderer, surface);

        SDL_SetTextureBlendMode(texture.get(), RGSS::Blend_Mode_Color);
        renderer.render(texture, cen::point(0, 0));

        return;
    }
    case Code::Drawable_Dispose:
    {
        uintptr_t id;
        int z;
        Pull id >> z;

        canvas.erase({z, id});
        return;
    }
    case Code::Drawable_Set_Z:
    {
        uintptr_t id;
        int old_z, new_z;
        Pull id >> old_z >> new_z;

        canvas.update({old_z, id}, new_z);
        return;
    }

    case Code::DrawableV_Dispose:
    {
        uintptr_t id, vid;
        int z, vz;
        Pull id >> z >> vid >> vz;

        auto child_canvas = canvas.find_by_key({vz, vid});
        if (child_canvas)
        {
            child_canvas->erase({z, id});
        }
        return;
    }
    case Code::DrawableV_Set_Z:
    {
        uintptr_t id, vid;
        int old_z, new_z, vz;
        Pull id >> old_z >> new_z >> vid >> vz;

        auto child_canvas = canvas.find_by_key({vz, vid});
        if (child_canvas)
        {
            child_canvas->update({old_z, id}, new_z);
        }
        return;
    }

#define SETUP(type)                   \
    RGSS::type *drawable;             \
    int z;                            \
    uintptr_t id;                     \
    Pull drawable >> z >> id;         \
    canvas.insert({z, id}, drawable); \
    return;

#define SETUP_V(type)                                  \
    RGSS::type *drawable;                              \
    int z, vz;                                         \
    uintptr_t id, vid;                                 \
    Pull drawable >> z >> id >> vz >> vid;             \
    auto child_canvas = canvas.find_by_key({vz, vid}); \
    if (child_canvas)                                  \
    {                                                  \
        child_canvas->insert({z, id}, drawable);       \
    }                                                  \
    return;

    case Code::Viewport_Setup:
    {
        SETUP(Viewport);
    }
    case Code::Sprite_Setup:
    {
        SETUP(Sprite)
    }
    case Code::SpriteV_Setup:
    {
        SETUP_V(Sprite);
    }
    case Code::Plane_Setup:
    {
        SETUP(Plane);
    }
    case Code::PlaneV_Setup:
    {
        SETUP_V(Plane);
    }
    case Code::Window_Setup:
    {
        SETUP(Window);
    }
    case Code::WindowV_Setup:
    {
        SETUP_V(Window);
    }
    case Code::Tilemap_Setup:
    {
        SETUP(Tilemap);
    }
    case Code::TilemapV_Setup:
    {
        SETUP_V(Tilemap);
    }

#undef SETUP
#undef SETUP_V
#undef Pull
    case Code::Graphics_Freeze:
    {
        // 1. 获取当前的画面，保存
        cen::texture current(
            renderer, window.get_pixel_format(),
            cen::texture_access::target, {window.width(), window.height()});

        renderer.set_target(current);
        draw();
        transition_freeze_id = object_id.fetch_add(1);
        renderer.reset_target();
        renderer.set_blend_mode(cen::blend_mode::none);
        renderer.render(current, cen::point(0, 0));

        textures.emplace(transition_freeze_id, move(current));
        return;
    }
    case Code::Graphics_Transition_Prepare:
    {
        std::string filename;
        pipe_render >> filename;

        transition_surface.reset();
        transition_surface = std::make_unique<cen::surface>(
            cen::surface(cen::surface(filename).convert(cen::pixel_format::rgba32)));
        return;
    }
    case Code::Graphics_Transition_Update:
    {
        int t, vague;
        pipe_render >> t >> vague;
        if (t == 0)
        {
            textures.erase(transition_freeze_id);
            transition_freeze_id = 0;
            transition_surface.reset();
            return;
        }
        // 1. 绘制新
        renderer.reset_target();
        draw();
        // 2. 创建目标，旧
        auto &texture = textures.at(transition_freeze_id);
        const int width = texture.width();
        const int height = texture.height();

        auto target = cen::texture(
            renderer, window.get_pixel_format(),
            cen::texture_access::target, {width, height});
        renderer.set_target(target);
        renderer.render(texture, cen::point(0, 0));
        if (transition_surface)
        {
            uint8_t *data = (uint8_t *)transition_surface->data();
            const int size = transition_surface->width() * transition_surface->height();
            if (vague)
            {
                for (int i = 0; i < size; i++)
                {
                    int y = (*data + t + vague - 255) * 255 / vague;
                    *(data + 3) = (y > 255) ? 255 : ((y < 0) ? 0 : y);
                    data += 4;
                }
            }
            else
            {
                for (int i = 0; i < size; i++)
                {
                    *(data + 3) = (*data + t > 255) ? 255 : 0;
                    data += 4;
                }
            }
            // set renderer only alpha
            auto texture_alpha = cen::texture(renderer, *transition_surface);
            SDL_SetTextureBlendMode(texture_alpha.get(), RGSS::Blend_Mode_Alpha);
            renderer.render(texture_alpha, cen::irect(0, 0, width, height));
        }
        else
        {
            target.set_alpha(t);
        }
        renderer.reset_target();
        target.set_blend_mode(cen::blend_mode::blend);
        renderer.render(target, cen::point(0, 0));
        return;
    }
    default:
        return;
    }
}

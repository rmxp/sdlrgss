#include "rgss.h"
#include "canvas.h"

namespace Core
{
    Canvas::Canvas(RGSS::Viewport *ptr)
    {
        viewport = ptr;
        tree = {};
    }
    Canvas::~Canvas()
    {
        for (auto pair : tree)
        {
            if (pair.second.first == Drawable::Canvas)
            {
                delete (Canvas *)(pair.second.second);
            }
        }
    }

    void Canvas::insert(z_index index, RGSS::Viewport *ptr)
    {
        tree.insert({index, {Drawable::Canvas, new Canvas(ptr)}});
    }

    void Canvas::insert(z_index index, RGSS::Sprite *ptr)
    {
        tree.insert({index, {Drawable::Sprite, ptr}});
    }

    void Canvas::insert(z_index index, RGSS::Plane *ptr)
    {
        tree.insert({index, {Drawable::Plane, ptr}});
    }

    void Canvas::insert(z_index index, RGSS::Window *ptr)
    {
        tree.insert({index, {Drawable::Window, ptr}});
    }

    void Canvas::insert(z_index index, RGSS::Tilemap *ptr)
    {
        for (int i = 0; i < 8; i++)
        {
            tree.insert({{index.first + i * 32, index.second}, {Drawable::Tilemap, ptr}});
        }
    }

    void Canvas::erase(z_index index)
    {
        auto it = tree.find(index);
        if (it != tree.end())
        {
            if (it->second.first == Drawable::Tilemap)
            {
                for (int i = 0; i < 8; i++)
                {
                    tree.erase({i * 32 + index.first, index.second});
                }
            }
            else
            {
                if (it->second.first == Drawable::Canvas)
                {
                    delete (Canvas *)(it->second.second);
                }
                tree.erase(it);
            }
        }
    }

    void Canvas::update(z_index index, int new_z)
    {
        auto it = tree.find(index);
        if (it != tree.end())
        {
            auto value = it->second;
            auto id = it->first.second;
            tree.erase(it);
            tree.insert({{new_z, id}, move(value)});
        }
    }

    Canvas *Canvas::find_by_viewport(RGSS::Viewport *ptr)
    {
        int z = ptr->z;
        uintptr_t id = ptr->id;
        return find_by_key({z, id});
    }

    Canvas *Canvas::find_by_key(z_index index)
    {
        auto it = tree.find(index);
        if (it != tree.end())
        {
            if (it->second.first == Drawable::Canvas)
            {
                return static_cast<Canvas *>(it->second.second);
            }
        }
        return nullptr;
    }
}
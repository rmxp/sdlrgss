#pragma once
#include "core.h"
#include "rgss.h"
#include "command_pipeline.h"
#include "timer.hpp"

class Graphics_Manager
{

private:
    // 单例类
    Graphics_Manager();
    ~Graphics_Manager() = default;
    Graphics_Manager(const Graphics_Manager &);
    Graphics_Manager &operator=(const Graphics_Manager &);

    std::unordered_map<uintptr_t, cen::texture> textures;

    cen::renderer_handle renderer;
    cen::window_handle window;

    Core::Command_Pipeline pipe_script;
    Core::Command_Pipeline pipe_render;

    Core::Canvas canvas;
    Core::Timer timer;

    std::unordered_map<std::string, std::map<int, cen::font>> render_fonts;
    std::unordered_map<std::string, std::map<int, TTF_Font *>> script_fonts;

    // pixel effects (on current render target)
    void apply_tone_gray(const RGSS::Tone &);
    void apply_tone_color(const RGSS::Tone &);
    void apply_color(const RGSS::Color &);

    // draw
    void draw();
    void draw(const Core::Canvas::canvas_tree &);
    void draw(const Core::Canvas *);
    void draw(RGSS::Sprite *);
    void draw(RGSS::Plane *);
    void draw(RGSS::Window *);
    void draw(RGSS::Tilemap *, const int);

public:
    static Graphics_Manager &getInstance();
    std::atomic<uintptr_t> object_id;

    void setup(const cen::window &, const cen::renderer &);
    void finalize();
    void run();
    bool idle();
    void tick(const int);

    Core::Command_Pipeline &operator<<(Core::Command_Code);
    void process_render_commands(Core::Command_Code);

    // transition
    uintptr_t transition_freeze_id;
    std::unique_ptr<cen::surface> transition_surface;

    // sdl_ttf不支持多线程，所以渲染线程使用的字体和脚本使用的字体要分开存储
    // 脚本里只有Bitmap#text_size，并不需要封装很多层，所以直接保存指针
    // 注意，理论上Bitmap#text_size不是线程安全的，但是Ruby有GIL，
    // 而C程序会独占GIL不被打断，所以可以多线程调用Bitmap#text_size
    // 参考：
    // (原)多线程sdl_ttf显示文字 - lihaiping - 博客园
    // https://www.cnblogs.com/lihaiping/p/4584125.html

    // font
    cen::font &font_for_render(const std::string &, int);
    TTF_Font *font_for_script(const std::string &, int);
};

#define GM (Graphics_Manager::getInstance())

# 参考文档
- openrgss：https://github.com/zh99998/OpenRGSS
- rice4 文档：https://jasonroelofs.com/rice/4.x/tutorial.html
- centurion 文档：https://albin-johansson.github.io/centurion/index.html
- centurion WIKI：https://github.com/albin-johansson/centurion/wiki
- rubyVM: https://stackoverflow.com/search?q=ruby_init
- RMXP帮助文档：[RPGXP.chm](./conf/RPGXP.chm)
- RMXP帮助文档（在线）：https://www.rpg-maker.fr/dl/monos/aide/xp/
- SDL中文教程 - 游戏编程入门：https://tjumyk.github.io/sdl-tutorial-cn/
- 没事造轮子SDL2：https://wangzhechao.com/sdl2/

# RGSS内置库
## 内部函数
- [x] load_data / save_data
- [ ] p / print


## 内部类
- [x] Color
- [x] Rect
- [x] Tone
- [x] Table
- [x] Font
- [x] Bitmap
  - [x] new / dispose
  - [x] blt / fill
  - [x] hue_change
  - [x] font / draw_text / text_size
  - [x] get_pixel / set_pixel -> `Palette`
- [x] Viewport
  - [x] new / dispose
  - [x] color / tone
  - [x] flash / update
- [x] Sprite
  - [x] new / dispose
  - [x] color / tone
  - [x] blend_type
  - [x] bush_depth
  - [x] flash / update
  - [x] mirror / flip / rotate
- [x] Plane
- [x] Tilemap
- [x] Window
- [ ] RGSSError
- [x] RPG::Sprite
- [x] RPG::Weather


## 内部模块
- [ ] Audio
- [x] Graphics
- [x] Input
- [x] RPG
- [x] RPG::Cache


# 编译
安装依赖：
1. 安装ruby，下载ruby-installer-devkit安装。一定要安装mingw编译环境。
2. 安装rice，使用ruby的gem安装rice：`apps\ruby\bin\gem install rice`
3. 安装SDL2，下载SDL2全家桶：SDL2/SDL2_image/SDL2_mixer/SDL2_ttf，解压
4. 安装Centurion，下载最新的release版本解压
5. 制作符号链接：
```cmd
cd apps
mklink /j ruby {Ruby安装路径，目录下有LICENSE.txt}
mklink /j sdl2 {SDL2安装路径，独立的4个文件夹，见Makefile}
mklink /j centurion {Centurion安装路径，目录下有LICENSE}
```
6. 安装了VSCode可以使用`Ctrl + Shift + B`编译。否则请参考`.vscode/task.json`里的任务和Makefile。

## 外部依赖版本
各外部依赖版本：

Library | Version 
:-:|:-:
[ruby](https://rubyinstaller.org/downloads/) | 3.0.2 [x64-mingw32]
[rice](https://github.com/jasonroelofs/rice/releases) | 4.0.2 
[centurion](https://github.com/albin-johansson/centurion/releases) | 6.2.0
[SDL](https://www.libsdl.org/download-2.0.php) | 2.0.16
[SDL image](https://www.libsdl.org/projects/SDL_image/) | 2.0.5
[SDL mixer](https://www.libsdl.org/projects/SDL_mixer/) | 2.0.4
[SDL ttf](https://www.libsdl.org/projects/SDL_ttf/) | 2.0.15

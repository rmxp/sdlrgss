#include "rgss_function.h"
#define VALUEx1 Arg("").isValue()
#define VALUEx2 Arg("").isValue(), VALUEx1
#define VALUEx3 Arg("").isValue(), VALUEx2
#define VALUEx4 Arg("").isValue(), VALUEx3
#define VALUEx5 Arg("").isValue(), VALUEx4
#define VALUEx6 Arg("").isValue(), VALUEx5
#define VALUEx7 Arg("").isValue(), VALUEx6
#define VALUEx8 Arg("").isValue(), VALUEx7

namespace RGSS
{
    void Color::set_red(VALUE self, VALUE red)
    {
        auto *color = static_cast<RGSS::Color *>(Rice::detail::unwrap(self));
        color->red = FIX2INT(red);
    }
    void Color::set_green(VALUE self, VALUE green)
    {
        auto *color = static_cast<RGSS::Color *>(Rice::detail::unwrap(self));
        color->green = FIX2INT(green);
    }
    void Color::set_blue(VALUE self, VALUE blue)
    {
        auto *color = static_cast<RGSS::Color *>(Rice::detail::unwrap(self));
        color->blue = FIX2INT(blue);
    }
    void Color::set_alpha(VALUE self, VALUE alpha)
    {
        auto *color = static_cast<RGSS::Color *>(Rice::detail::unwrap(self));
        color->alpha = FIX2INT(alpha);
    }
    void Color::set_attrs(VALUE self, VALUE red, VALUE green, VALUE blue, VALUE alpha)
    {
        auto *color = static_cast<RGSS::Color *>(Rice::detail::unwrap(self));
        color->red = FIX2INT(red);
        color->green = FIX2INT(green);
        color->blue = FIX2INT(blue);
        color->alpha = FIX2INT(alpha);
    }
    void Color::init_wrapper()
    {
        using namespace Rice;

        define_class<Color>("Color")
            .define_constructor(Constructor<Color>())
            .define_function("set_red", &Color::set_red, VALUEx2)
            .define_function("set_green", &Color::set_green, VALUEx2)
            .define_function("set_blue", &Color::set_blue, VALUEx2)
            .define_function("set_alpha", &Color::set_alpha, VALUEx2)
            .define_function("set_attrs", &Color::set_attrs, VALUEx5)
            .define_singleton_method(
                "_new", [](Class _class)
                { return Color(); });
    }

    void Tone::set_red(VALUE self, VALUE red)
    {
        auto *tone = static_cast<RGSS::Tone *>(Rice::detail::unwrap(self));
        tone->red = FIX2INT(red);
    }
    void Tone::set_green(VALUE self, VALUE green)
    {
        auto *tone = static_cast<RGSS::Tone *>(Rice::detail::unwrap(self));
        tone->green = FIX2INT(green);
    }
    void Tone::set_blue(VALUE self, VALUE blue)
    {
        auto *tone = static_cast<RGSS::Tone *>(Rice::detail::unwrap(self));
        tone->blue = FIX2INT(blue);
    }
    void Tone::set_gray(VALUE self, VALUE gray)
    {
        auto *tone = static_cast<RGSS::Tone *>(Rice::detail::unwrap(self));
        tone->gray = FIX2INT(gray);
    }
    void Tone::set_attrs(VALUE self, VALUE red, VALUE green, VALUE blue, VALUE gray)
    {
        auto *tone = static_cast<RGSS::Tone *>(Rice::detail::unwrap(self));
        tone->red = FIX2INT(red);
        tone->green = FIX2INT(green);
        tone->blue = FIX2INT(blue);
        tone->gray = FIX2INT(gray);
    }
    void Tone::init_wrapper()
    {
        using namespace Rice;

        define_class<Tone>("Tone")
            .define_constructor(Constructor<Tone>())
            .define_function("set_red", &Tone::set_red, VALUEx2)
            .define_function("set_green", &Tone::set_green, VALUEx2)
            .define_function("set_blue", &Tone::set_blue, VALUEx2)
            .define_function("set_gray", &Tone::set_gray, VALUEx2)
            .define_function("set_attrs", &Tone::set_attrs, VALUEx5)
            .define_singleton_method(
                "_new", [](Class _class)
                { return Tone(); });
    }

    void Rect::set_x(VALUE self, VALUE x)
    {
        auto *rect = static_cast<RGSS::Rect *>(Rice::detail::unwrap(self));
        rect->x = FIX2INT(x);
    }
    void Rect::set_y(VALUE self, VALUE y)
    {
        auto *rect = static_cast<RGSS::Rect *>(Rice::detail::unwrap(self));
        rect->y = FIX2INT(y);
    }
    void Rect::set_width(VALUE self, VALUE width)
    {
        auto *rect = static_cast<RGSS::Rect *>(Rice::detail::unwrap(self));
        rect->width = FIX2INT(width);
    }
    void Rect::set_height(VALUE self, VALUE height)
    {
        auto *rect = static_cast<RGSS::Rect *>(Rice::detail::unwrap(self));
        rect->height = FIX2INT(height);
    }
    void Rect::set_attrs(VALUE self, VALUE x, VALUE y, VALUE width, VALUE height)
    {
        auto *rect = static_cast<RGSS::Rect *>(Rice::detail::unwrap(self));
        rect->x = FIX2INT(x);
        rect->y = FIX2INT(y);
        rect->width = FIX2INT(width);
        rect->height = FIX2INT(height);
    }
    void Rect::init_wrapper()
    {
        using namespace Rice;

        define_class<Rect>("Rect")
            .define_constructor(Constructor<Rect>())
            .define_function("set_x", &Rect::set_x, VALUEx2)
            .define_function("set_y", &Rect::set_y, VALUEx2)
            .define_function("set_width", &Rect::set_width, VALUEx2)
            .define_function("set_height", &Rect::set_height, VALUEx2)
            .define_function("set_attrs", &Rect::set_attrs, VALUEx5)
            .define_singleton_method(
                "_new", [](Class _class)
                { return Rect(); });
    }

    void Table::set_size(VALUE self, VALUE xsize, VALUE ysize, VALUE zsize)
    {
        auto *table = static_cast<RGSS::Table *>(Rice::detail::unwrap(self));
        table->resize(FIX2INT(xsize), FIX2INT(ysize), FIX2INT(zsize));
    }
    VALUE Table::get(VALUE self, VALUE index)
    {
        auto *table = static_cast<RGSS::Table *>(Rice::detail::unwrap(self));
        return INT2FIX(table->get_index(FIX2INT(index)));
    }
    void Table::set(VALUE self, VALUE index, VALUE value)
    {
        auto *table = static_cast<RGSS::Table *>(Rice::detail::unwrap(self));
        table->set_index(FIX2INT(index), FIX2INT(value));
    }
    void Table::init_wrapper()
    {
        using namespace Rice;

        define_class<Table>("Table")
            .define_constructor(Constructor<Table>())
            .define_function("get", &Table::get, VALUEx2, Return().isValue())
            .define_function("set", &Table::set, VALUEx3)
            .define_function("set_size", &Table::set_size, VALUEx4)
            .define_method("_dump", &Table::_dump, Arg("depth") = 0)
            .define_singleton_method(
                "_new", [](Class _class, std::string s)
                { return Table(s); });
    }

    void Font::set_path(VALUE self, std::string path)
    {
        auto *font = static_cast<RGSS::Font *>(Rice::detail::unwrap(self));
        font->path = path;
    }
    void Font::set_size(VALUE self, VALUE size)
    {
        auto *font = static_cast<RGSS::Font *>(Rice::detail::unwrap(self));
        font->size = FIX2INT(size);
    }
    void Font::set_bold(VALUE self, VALUE bold)
    {
        auto *font = static_cast<RGSS::Font *>(Rice::detail::unwrap(self));
        font->bold = RTEST(bold);
    }
    void Font::set_italic(VALUE self, VALUE italic)
    {
        auto *font = static_cast<RGSS::Font *>(Rice::detail::unwrap(self));
        font->italic = RTEST(italic);
    }
    void Font::set_color(VALUE self, VALUE color)
    {
        auto *font = static_cast<RGSS::Font *>(Rice::detail::unwrap(self));
        auto *c = static_cast<RGSS::Color *>(Rice::detail::unwrap(color));
        font->color->set(c->red, c->green, c->blue, c->alpha);
    }
    void Font::init_wrapper()
    {
        using namespace Rice;

        define_class<Font>("Font")
            .define_constructor(Constructor<Font, std::string, int>())
            // class variables
            .define_singleton_attr("default_path", &Font::default_path)
            .define_singleton_attr("default_size", &Font::default_size)
            .define_singleton_attr("default_bold", &Font::default_bold)
            .define_singleton_attr("default_italic", &Font::default_italic)
            .define_singleton_attr("default_color", &Font::default_color, AttrAccess::Read)
            .define_attr("color", &Font::color, AttrAccess::Read)
            .define_function("set_path", &Font::set_path, VALUEx1, Arg("path"))
            .define_function("set_size", &Font::set_size, VALUEx2)
            .define_function("set_bold", &Font::set_bold, VALUEx2)
            .define_function("set_italic", &Font::set_italic, VALUEx2)
            .define_function("set_color", &Font::set_color, VALUEx2);
    }

    void Bitmap::_setup_path(VALUE self, std::string path, VALUE width, VALUE height)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->setup(std::move(path), FIX2INT(width), FIX2INT(height));
    }
    void Bitmap::_setup_canvas(VALUE self, VALUE width, VALUE height)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->setup_target(FIX2INT(width), FIX2INT(height));
    }
    void Bitmap::_dispose(VALUE self)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->dispose();
    }
    void Bitmap::_blt(VALUE self, VALUE x, VALUE y, VALUE src_bitmap, VALUE rect, VALUE opacity)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        auto *src_ptr = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(src_bitmap));
        auto *rect_ptr = static_cast<RGSS::Rect *>(Rice::detail::unwrap(rect));
        bitmap->blt(FIX2INT(x), FIX2INT(y), src_ptr, rect_ptr, FIX2INT(opacity));
    }
    void Bitmap::_stretch_blt(VALUE self, VALUE dest_rect, VALUE src_bitmap, VALUE src_rect, VALUE opacity)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        auto *dest = static_cast<RGSS::Rect *>(Rice::detail::unwrap(dest_rect));
        auto *src_ptr = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(src_bitmap));
        auto *src = static_cast<RGSS::Rect *>(Rice::detail::unwrap(src_rect));
        bitmap->stretch_blt(dest, src_ptr, src, FIX2INT(opacity));
    }
    void Bitmap::_fill_rect(VALUE self, VALUE x, VALUE y, VALUE width, VALUE height, VALUE color)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        auto *color_ptr = static_cast<RGSS::Color *>(Rice::detail::unwrap(color));
        bitmap->fill_rect(FIX2INT(x), FIX2INT(y), FIX2INT(width), FIX2INT(height), color_ptr);
    }
    void Bitmap::_clear(VALUE self)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->clear();
    }
    void Bitmap::_draw_text(VALUE self, VALUE x, VALUE y, VALUE width, VALUE height, std::string str, VALUE align)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->draw_text(FIX2INT(x), FIX2INT(y), FIX2INT(width), FIX2INT(height), std::move(str), FIX2INT(align));
    }
    void Bitmap::_text_size(VALUE self, std::string str)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->text_size(std::move(str));
    }
    void Bitmap::_hue_change(VALUE self, VALUE hue)
    {
        auto *bitmap = static_cast<RGSS::Bitmap *>(Rice::detail::unwrap(self));
        bitmap->hue_change(FIX2INT(hue));
    }
    void Bitmap::init_wrapper()
    {
        using namespace Rice;

        define_class<Bitmap>("Bitmap")
            // constructor and distructor
            .define_constructor(Constructor<Bitmap>())
            .define_attr("font", &Bitmap::font, AttrAccess::Read)
            .define_function("_setup_path", &Bitmap::_setup_path, VALUEx1, Arg("path"), VALUEx2)
            .define_function("_setup_canvas", &Bitmap::_setup_canvas, VALUEx3)
            .define_function("_dispose", &Bitmap::_dispose, VALUEx1)
            .define_function("_blt", &Bitmap::_blt, VALUEx3.keepAlive(), VALUEx3)
            .define_function("_stretch_blt", &Bitmap::_stretch_blt, VALUEx3.keepAlive(), VALUEx2)
            .define_function("_fill_rect", &Bitmap::_fill_rect, VALUEx6)
            .define_function("_clear", &Bitmap::_clear, VALUEx1)
            .define_function("_draw_text", &Bitmap::_draw_text, VALUEx5, Arg("str"), VALUEx1 = INT2FIX(0))
            .define_function("_text_size", &Bitmap::_text_size, VALUEx1, Arg("str"))
            .define_function("_hue_change", &Bitmap::_hue_change, VALUEx2)
            .define_method("_clone", &Bitmap::clone, Return().takeOwnership());
    }
}

#undef VALUEx1
#undef VALUEx2
#undef VALUEx3
#undef VALUEx4
#undef VALUEx5
#undef VALUEx6
#undef VALUEx7
#undef VALUEx8
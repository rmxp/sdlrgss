# encoding: utf-8

def test_loop(n)
  p "test loops #{Graphics.synchronize ? "(sync)" : "(async)"}"
  t = Time.now
  n.times do |i|
    Graphics.update
    Input.update
    i += 1
    yield i if block_given?
  end
  puts "Frames: %d, FPS: %.1f" % [n, n / (Time.now - t)]
end

BEGIN { puts "-- test --" }
END { puts "-- end --" }

def rgss_main
  script = load_data("Data/Scripts.rxdata")
  script.each do |id, title, data|
    if title == "Main"
      require "core/rpgxp_plugin"
    end
    eval Zlib::Inflate.inflate(data).force_encoding("utf-8"), nil, title
  end
end

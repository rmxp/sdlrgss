#include "rgss_wrapper.h"

namespace RGSSWrapper
{
    using namespace Rice;
    using namespace RGSS;
    using Code = Core::Command_Code;

    void initialize()
    {
        // Core
        Module rb_mCore =
            define_module("Core")
                .const_set("Version", detail::to_ruby(PROGRAM_VERSION))
                .define_module_function("log_level=", &Core::set_log_level);

        Color::init_wrapper();
        Tone::init_wrapper();
        Rect::init_wrapper();
        Table::init_wrapper();
        Font::init_wrapper();
        Bitmap::init_wrapper();

        // Palette
        Data_Type<Palette> rb_cPalette =
            define_class<Palette>("Palette")
                // constructor and distructor
                .define_constructor(Constructor<Palette>())
                .define_method("_setup", &Palette::setup)
                .define_method("_setup2", &Palette::setup2)
                .define_method("disposed?", &Palette::is_disposed)
                .define_method("dispose", &Palette::dispose)
                // attributes
                .define_attr("id", &Palette::id, AttrAccess::Read)
                .define_attr("width", &Palette::width, AttrAccess::Read)
                .define_attr("height", &Palette::height, AttrAccess::Read)
                // methods
                .define_method("set_pixel", &Palette::set_pixel)
                .define_method("get_pixel", &Palette::get_pixel);

        // Autotiles
        Data_Type<Autotiles> rb_cAutotiles =
            define_class<Autotiles>("Autotiles")
                .define_constructor(Constructor<Autotiles>())
                .define_method("[]", &Autotiles::get)
                .define_method("[]=", &Autotiles::set, Arg("index"), Arg("bitmap").keepAlive());
        // Drawable
        Data_Type<Drawable> rb_cDrawable =
            define_class<Drawable>("Drawable")
                // attributes
                .define_attr("visible", &Drawable::visible)
                .define_attr("z", &Drawable::z, AttrAccess::Read)
                .define_method("z=", &Drawable::set_z)
                .define_attr("id", &Drawable::id, AttrAccess::Read)
                // methods
                .define_method("dispose", &Drawable::dispose)
                .define_method("disposed?", &Drawable::is_disposed);
        // Viewport
        Data_Type<Viewport>
            rb_cViewport =
                define_class<Viewport, Drawable>("Viewport")
                    // constructor and distructor
                    .define_constructor(Constructor<Viewport>())
                    .define_method("_setup", &Viewport::setup)
                    // attributes
                    .define_attr("rect", &Viewport::rect, AttrAccess::Read)
                    .define_attr("ox", &Viewport::ox)
                    .define_attr("oy", &Viewport::oy)
                    .define_attr("color", &Viewport::color, AttrAccess::Read)
                    .define_attr("tone", &Viewport::tone, AttrAccess::Read)
                    // methods
                    .define_method("_flash", &Viewport::flash)
                    .define_method("_flash2", &Viewport::flash2)
                    .define_method("update", &Viewport::update);
        // Drawable2
        rb_cDrawable.define_attr("viewport", &Drawable::viewport, AttrAccess::Read);
        // Sprite
        Data_Type<Sprite>
            rb_cSprite =
                define_class<Sprite, Drawable>("Sprite")
                    // constructor and distructor
                    .define_constructor(Constructor<Sprite, std::optional<Viewport *>>(), Arg("viewport") = std::nullopt)
                    // attributes
                    .define_attr("bitmap", &Sprite::bitmap, AttrAccess::Read)
                    .define_method("bitmap=", &Sprite::set_bitmap, Arg("bitmap").keepAlive())
                    .define_attr("src_rect", &Sprite::src_rect, AttrAccess::Read)
                    .define_attr("x", &Sprite::x)
                    .define_attr("y", &Sprite::y)
                    .define_attr("ox", &Sprite::ox)
                    .define_attr("oy", &Sprite::oy)
                    .define_attr("zoom_x", &Sprite::zoom_x)
                    .define_attr("zoom_y", &Sprite::zoom_y)
                    .define_attr("angle", &Sprite::angle)
                    .define_attr("mirror", &Sprite::mirror)
                    .define_attr("bush_depth", &Sprite::bush_depth)
                    .define_attr("opacity", &Sprite::opacity, AttrAccess::Read)
                    .define_method("opacity=", &Sprite::set_opacity)
                    .define_attr("blend_type", &Sprite::blend_type)
                    .define_attr("color", &Sprite::color, AttrAccess::Read)
                    .define_attr("tone", &Sprite::tone, AttrAccess::Read)
                    // methods
                    .define_method("_flash", &Sprite::flash)
                    .define_method("_flash2", &Sprite::flash2)
                    .define_method("update", &Sprite::update);

        // Plane
        Data_Type<Plane> rb_cPlane =
            define_class<Plane, Drawable>("Plane")
                // constructor and distructor
                .define_constructor(Constructor<Plane, std::optional<Viewport *>>(), Arg("viewport") = std::nullopt)
                // attributes
                .define_attr("bitmap", &Plane::bitmap, AttrAccess::Read)
                .define_method("bitmap=", &Plane::set_bitmap, Arg("bitmap").keepAlive())
                .define_attr("ox", &Plane::ox)
                .define_attr("oy", &Plane::oy)
                .define_attr("zoom_x", &Plane::zoom_x)
                .define_attr("zoom_y", &Plane::zoom_y)
                .define_attr("opacity", &Plane::opacity, AttrAccess::Read)
                .define_method("opacity=", &Plane::set_opacity)
                .define_attr("blend_type", &Plane::blend_type)
                .define_attr("color", &Plane::color, AttrAccess::Read)
                .define_attr("tone", &Plane::tone, AttrAccess::Read);
        // Window
        Data_Type<Window> rb_cWindow =
            define_class<Window, Drawable>("Window")
                // constructor and distructor
                .define_constructor(Constructor<Window, std::optional<Viewport *>>(), Arg("viewport") = std::nullopt)
                // attributes
                .define_attr("windowskin", &Window::window_skin, AttrAccess::Read)
                .define_method("windowskin=", &Window::set_windowskin, Arg("windowskin").keepAlive())
                .define_attr("contents", &Window::contents, AttrAccess::Read)
                .define_method("contents=", &Window::set_contents, Arg("contents").keepAlive())
                .define_attr("stretch", &Window::stretch)
                .define_attr("cursor_rect", &Window::cursor_rect, AttrAccess::Read)
                .define_attr("active", &Window::active)
                .define_attr("visible", &Window::visible)
                .define_attr("pause", &Window::pause)
                .define_attr("x", &Window::x)
                .define_attr("y", &Window::y)
                .define_attr("width", &Window::width)
                .define_attr("height", &Window::height)
                .define_attr("ox", &Window::ox)
                .define_attr("oy", &Window::oy)
                .define_attr("opacity", &Window::opacity, AttrAccess::Read)
                .define_method("opacity=", &Window::set_opacity)
                .define_attr("back_opacity", &Window::back_opacity, AttrAccess::Read)
                .define_method("back_opacity=", &Window::set_back_opacity)
                .define_attr("contents_opacity", &Window::contents_opacity, AttrAccess::Read)
                .define_method("contents_opacity=", &Window::set_contents_opacity)
                // methods
                .define_method("update", &Window::update);

        // Tilemap
        Data_Type<Tilemap> rb_cTilemap =
            define_class<Tilemap, Drawable>("Tilemap")
                // constructor and distructor
                .define_constructor(Constructor<Tilemap, std::optional<Viewport *>>(), Arg("viewport") = std::nullopt)
                // attributes
                .define_attr("ox", &Tilemap::ox)
                .define_attr("oy", &Tilemap::oy)
                .define_attr("autotiles", &Tilemap::autotiles, AttrAccess::Read)
                .define_attr("tileset", &Tilemap::tileset, AttrAccess::Read)
                .define_method("tileset=", &Tilemap::set_tileset, Arg("tileset").keepAlive())
                .define_attr("map_data", &Tilemap::map_data, AttrAccess::Read)
                .define_method("map_data=", &Tilemap::set_map_data, Arg("map_data").keepAlive())
                .define_attr("flash_data", &Tilemap::flash_data, AttrAccess::Read)
                .define_method("flash_data=", &Tilemap::set_flash_data, Arg("flash_data").keepAlive())
                .define_attr("priorities", &Tilemap::priorities, AttrAccess::Read)
                .define_method("priorities=", &Tilemap::set_priorities, Arg("priorities").keepAlive())
                // methods
                .define_method("update", &Tilemap::update);
        // Audio
        Module rb_mAudio =
            define_module("Audio")
                .define_module_function("_bgm_play", &Audio::bgm_play)
                .define_module_function("bgm_stop", &Audio::bgm_stop)
                .define_module_function("bgm_fade", &Audio::bgm_fade)
                .define_module_function("_bgs_play", &Audio::bgs_play)
                .define_module_function("bgs_stop", &Audio::bgs_stop)
                .define_module_function("bgs_fade", &Audio::bgs_fade)
                .define_module_function("_me_play", &Audio::me_play)
                .define_module_function("me_stop", &Audio::me_stop)
                .define_module_function("me_fade", &Audio::me_fade)
                .define_module_function("_se_play", &Audio::se_play)
                .define_module_function("se_stop", &Audio::se_stop);

        // Graphics
        Module rb_mGraphics =
            define_module("Graphics")
                .define_module_function("update", &Graphics::update)
                .define_module_function("freeze", &Graphics::freeze)
                .define_module_function("_transition", &Graphics::transition)
                .define_module_function("frame_reset", &Graphics::frame_reset)
                .define_module_function("frame_count", &Graphics::get_frame_count)
                .define_module_function("frame_count=", &Graphics::set_frame_count)
                .define_module_function("frame_rate", &Graphics::get_frame_rate)
                .define_module_function("frame_rate=", &Graphics::set_frame_rate)
                .define_module_function("synchronize", &Graphics::get_synchronize)
                .define_module_function("synchronize=", &Graphics::set_synchronize);

        // Input
        Module rb_mInput =
            define_module("Input")
                .define_module_function("update", &Input::update)
                .define_module_function("trigger?", &Input::is_trigger)
                .define_module_function("press?", &Input::is_press)
                .define_module_function("repeat?", &Input::is_repeat)
                .define_module_function("dir4", &Input::dir4)
                .define_module_function("dir8", &Input::dir8)
                // const
                .const_set("DOWN", INT2FIX(Input::DOWN))
                .const_set("LEFT", INT2FIX(Input::LEFT))
                .const_set("RIGHT", INT2FIX(Input::RIGHT))
                .const_set("UP", INT2FIX(Input::UP))
                .const_set("A", INT2FIX(Input::A))
                .const_set("B", INT2FIX(Input::B))
                .const_set("C", INT2FIX(Input::C))
                .const_set("X", INT2FIX(Input::X))
                .const_set("Y", INT2FIX(Input::Y))
                .const_set("Z", INT2FIX(Input::Z))
                .const_set("L", INT2FIX(Input::L))
                .const_set("R", INT2FIX(Input::R))
                .const_set("SHIFT", INT2FIX(Input::SHIFT))
                .const_set("CTRL", INT2FIX(Input::CTRL))
                .const_set("ALT", INT2FIX(Input::ALT))
                .const_set("F5", INT2FIX(Input::F5))
                .const_set("F6", INT2FIX(Input::F6))
                .const_set("F7", INT2FIX(Input::F7))
                .const_set("F8", INT2FIX(Input::F8))
                .const_set("F9", INT2FIX(Input::F9));
    }
}
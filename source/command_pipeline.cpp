#include "command_pipeline.h"

// -----------------------------------------------------
// Pipelines
// -----------------------------------------------------
/* pipeline class */
namespace Core
{
    // -----------------------------------------------------
    // Command_Pipeline
    // -----------------------------------------------------
    Command_Pipeline::Command_Pipeline()
    {
        constexpr int init_size = 4096;
        codes.reserve(init_size);
        args_ptr.reserve(init_size);
        args_int.reserve(init_size);
        args_double.reserve(init_size);
        args_string.reserve(init_size);
        reset();
    }

    void Command_Pipeline::reset()
    {
        if (args_ptr.size() != index_ptr)
        {
            cen::log::error("Pipe [id] is not empty, %d of %d", args_ptr.size(), index_ptr);
        }
        if (args_int.size() != index_int)
        {
            cen::log::error("Pipe [int] is not empty, %d of %d", args_int.size(), index_int);
        }
        if (args_double.size() != index_double)
        {
            cen::log::error("Pipe [double] is not empty, %d of %d", args_double.size(), index_double);
        }
        if (args_string.size() != index_string)
        {
            cen::log::error("Pipe [string] is not empty, %d of %d", args_string.size(), index_string);
        }

        codes.clear();
        args_ptr.clear();
        args_int.clear();
        args_double.clear();
        args_string.clear();
        index_ptr = 0;
        index_int = 0;
        index_double = 0;
        index_string = 0;
    }

    bool Command_Pipeline::idle()
    {
        return codes.empty();
    }

    Command_Pipeline &Command_Pipeline::operator>>(uintptr_t &value)
    {
        value = args_ptr[index_ptr++];
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator>>(int &value)
    {
        value = args_int[index_int++];
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator>>(double &value)
    {
        value = args_double[index_double++];
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator>>(std::string &value)
    {
        value = args_string[index_string++];
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator<<(const uintptr_t value)
    {
        args_ptr.push_back(value);
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator<<(const int value)
    {
        args_int.push_back(value);
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator<<(const double value)
    {
        args_double.push_back(value);
        return *this;
    }

    Command_Pipeline &Command_Pipeline::operator<<(const std::string value)
    {
        args_string.push_back(std::move(value));
        return *this;
    }
}
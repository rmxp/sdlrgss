#pragma once
#include "core.h"

namespace Event
{
    typedef cen::event_dispatcher<
        cen::quit_event,
        cen::window_event,
        cen::keyboard_event,
        cen::mouse_button_event>
        event_dispatcher_t;

    event_dispatcher_t create_event_dispatcher();

    void update();
    int key_state(const cen::key_code);
}